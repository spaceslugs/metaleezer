//
//  XTActionButton.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/9/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import UIKit

class XTActionButon: UIButton {
    private var _onPress: (() -> ())?
    
    func onPress(_ f: @escaping (() -> ()) ) {
        self.addTarget(self, action: #selector(onPressAction), for: .touchUpInside)
        self._onPress = f
    }
        
    @objc func onPressAction(_ sender: UIButton) {
        self._onPress?()
    }
    
}
