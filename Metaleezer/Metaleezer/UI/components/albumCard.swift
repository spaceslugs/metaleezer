//
//  albumCard.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/10/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import UIKit
import Stevia
import Nuke


class AlbumCard: UICollectionViewCell {
    
    let cover = UIImageView()
    let title = UILabel()
    let subtitle = UILabel()
    let activityIndicator = UIActivityIndicatorView()

    var album: AlbumExtended?
    
    var coverUrl: AppStateProp<String?>? = nil
    var coverLoading: AppStateProp<Bool?>? = nil

    
    override var reuseIdentifier: String? {
        return "AlbumCard"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    override func prepareForReuse() {
        super.prepareForReuse()
        coverUrl?.unsubscribe()
        coverUrl = nil
        coverLoading?.unsubscribe()
        coverLoading = nil
        cover.style(self.placeholderStyle)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let theme = System.theme()
         
         /// 🌳
         subviews {
            cover.subviews {
                activityIndicator
            }
            title
            subtitle
            
         }
         
         /// 📐
         let s = theme.spacing(.s)
        
         cover.heightEqualsWidth()
        
         activityIndicator.size(32)
         activityIndicator.centerInContainer()
        
         layout {
             0
             |cover|
             s
             |title-s-|
             s/2
             |subtitle-s-|
         }
         
        /// 🎨
        backgroundColor =  theme.color[.background1]
        
        cover.style(self.placeholderStyle).style { (l) in
            l.image = self.placeholder()
            l.backgroundColor = theme.color[.background2]
            l.contentMode = .center
            l.layer.cornerRadius = theme.borderRadius(.m)
            l.clipsToBounds = true
         }

        title.style { (l) in
           l.text = "Title"
           l.textColor = theme.color[.primary1]
           l.font = theme.textFont(.h2, font: .light)
           l.textAlignment = .left
           l.numberOfLines = 2
           l.lineBreakMode = .byWordWrapping
        }

        subtitle.style { (l) in
           l.text = "Subtitle"
           l.textColor = theme.color[.primary2]
           l.font = theme.textFont(.h2, font: .light)
           l.textAlignment = .left
           l.numberOfLines = 2
           l.lineBreakMode = .byWordWrapping
        }
        
        activityIndicator.style {
           $0.style = .large
           $0.color = theme.color[.primary3]
        }
 
         
    }
    
    func placeholder() -> UIImage {
        let theme = System.theme()
        return UIImage.icon(code: theme.glyph(XTGlyph.placeHolder), font:theme.iconFont(.h6, font: .light), textColor: theme.color[.primary2])
    }
    
    func placeholderStyle(l: UIImageView) {
        l.image = placeholder()
        l.contentMode = .center
    }
    
    func didUpdate(album: AlbumExtended?) {
        guard let id = album?.album.primaryId else {
            return
        }

        self.album = album
        
        /// new method!
        AppStore.dispatch(CoverFlow.fetchCoverUrl(albumId: id, album: album?.album.name, band: album?.band?.name))
        
        if coverUrl == nil {
            coverUrl = Prop { state in state.shared.coverUrl[id]?.url }
            coverLoading = Prop { state in state.shared.coverUrl[id]?.loading }
        }
        
        coverUrl?.didUpdate {
            if let urlString = $0, let url = URL(string: urlString) {
                self.cover.contentMode = .scaleAspectFit
                Nuke.loadImage(with: url, options: ImageLoadingOptions(placeholder: self.placeholder()), into: self.cover)
            }
        }
        
        coverLoading?.didUpdate { loading in
            (loading ?? false) == true ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
        }
                    
        
        title.style { (l) in
            l.text = album?.album.name
        }

        subtitle.style { (l) in
            ///l.text = "\(album?.album.date?.toDate()?.toString() ?? "")"
            l.text = album?.band?.name
        }
        
    }
    
    
}
