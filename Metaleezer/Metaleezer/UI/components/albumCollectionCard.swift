//
//  albumCollectionCard.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/10/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import UIKit
import Stevia


class AlbumCollectionCard: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    let icon = UILabel()
    let title = UILabel()
    let iconSeeAll = UILabel()
    
    var albums: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        return UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
    }()
    
    var collection: AlbumCollection?
    
    override var reuseIdentifier: String? {
        return "AlbumCollectionCard"
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        albums.dataSource = self
        albums.delegate = self
        albums.register(AlbumCard.self, forCellWithReuseIdentifier: "AlbumCard")
        
        let theme = System.theme()
        
        /// 🌳
        subviews {
            icon
            title
            iconSeeAll
            albums
        }
        
        /// 📐
        let s: CGFloat = CGFloat(theme.spacing(.s))
        let m: CGFloat = CGFloat(theme.spacing(.m))
        
        iconSeeAll.size(20)
        
        layout {
            s
            |-m--icon.size(20)--s--title--s--iconSeeAll--(>=m)-|
            m
            |albums|
            0
        }
        
        /// 🎨
        backgroundColor =  theme.color[.background1]
        
        albums.style { (l) in
            l.backgroundColor = theme.color[.background1]
        }
        
        title.style { (l) in
            l.text = "Collection"
            l.textColor = theme.color[.primary3]
            l.font = theme.textFont(.h4, font: .bold)
            l.textAlignment = .left
        }
        
        icon.style { (l) in
            l.text = "\u{f7e4}" ///theme.glyph(.albumCollection)
            l.textColor = theme.color[.primary3]
            l.font = theme.iconFont(.h3, font: .regular)
            l.textAlignment = .left
        }
        
        iconSeeAll.style { (l) in
            l.text = "\u{f054}" ///theme.glyph(.seeAll)
            l.textColor = theme.color[.primary3]
            l.font = theme.iconFont(.h2, font: .solid)
            l.textAlignment = .left
        }
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func didUpdate(collection: AlbumCollection) {
        self.collection = collection
        title.style { (l) in
            l.text = collection.title
        }
        
        icon.style { (l) in
            l.text = "\u{f7e4}"
        }
        
        albums.reloadData()
    }
    
    
    // MARK: collection view delegate
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collection?.albums.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlbumCard", for: indexPath)
        
        if let cell = cell as? AlbumCard {
            cell.didUpdate(album: self.collection?.albums[indexPath.item])
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 0.60 * collectionView.frame.size.height, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        let theme = System.theme()
        return CGFloat(theme.spacing(.m))
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let theme = System.theme()
        return UIEdgeInsets(top: 0, left: CGFloat(theme.spacing(.m)), bottom: 0, right: CGFloat(theme.spacing(.m)))
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        AppStore.dispatch(AlbumFlow.open(albumExtended: collection?.albums[indexPath.item],from: findViewController()?.navigationController))
    }
    
}
