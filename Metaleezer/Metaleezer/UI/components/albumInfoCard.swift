//
//  albumInfoCard.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/15/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import UIKit
import Stevia
import Nuke


class AlbumInfoCard: UICollectionViewCell {
    
    let activityIndicator = UIActivityIndicatorView()
    
    let content = UIView()
    
    let cover = UIImageView()
    let title = UILabel()
    let subtitle = UILabel()
    
    let releaseDateIcon = UILabel()
    let releaseDate = UILabel()
    
    let actionBar = UIView()
    let actionPlay = XTActionButon()
    let actionShare = XTActionButon()
    let actionLike = XTToggleButon()
    let actionNotifyMe = XTToggleButon()


    var album: AlbumExtended?
    
    var coverUrl: AppStateProp<String?>? = nil
    var coverLoading: AppStateProp<Bool?>? = nil
    
    var playing: AppStateProp<Bool>? = nil
    
    override var reuseIdentifier: String? {
        return "AlbumInfoCard"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let theme = System.theme()
        
        /// 🌳
        subviews {
            content.subviews {
                cover.subviews {
                    activityIndicator
                }
                title
                subtitle
                releaseDateIcon
                releaseDate
                actionBar.subviews {
                    actionPlay
                    actionLike
                    actionShare
                    actionNotifyMe
                }
            }
        }
        
        /// 📐
        let m = theme.spacing(.s)
        let l = theme.spacing(.l)
        let xl = theme.spacing(.xl)
        
        activityIndicator.size(32)
        activityIndicator.centerInContainer()
        
        content.fillContainer()
        
        cover.width(200).heightEqualsWidth().centerHorizontally()
        
        let btnSize = 48
        
        actionPlay.size(btnSize)
        actionLike.size(btnSize)
        actionShare.size(btnSize)
        actionNotifyMe.size(btnSize)
        
        actionBar.centerHorizontally()
        actionBar.layout {
            0
            |actionPlay--xl--actionLike--xl--actionShare--xl--actionNotifyMe|
            0
        }
        
        actionBar.bottom(m)
        
        releaseDate.centerHorizontally()
        releaseDateIcon.Right == releaseDate.Left - m
        align(horizontally: releaseDate,releaseDateIcon)
        
        content.layout {
            l
            cover
            l
            |-m-title-m-|
            m
            |-m-subtitle-m-|
            m
            releaseDate
        }
        
        /// 🎨
        content.backgroundColor =  theme.color[.background2]
        

        activityIndicator.style {
            $0.style = .large
            $0.color = theme.color[.primary3]
        }
        
        cover.style(self.placeholderStyle).style { (l) in
            l.image = UIImage.icon(code: theme.glyph(XTGlyph.placeHolder), font:theme.iconFont(.h6, font: .light), textColor: theme.color[.primary2])
            l.backgroundColor = theme.color[.background2]
            l.contentMode = .center
            l.layer.cornerRadius = theme.borderRadius(.m)
            l.clipsToBounds = true
        }
        
        title.style { (l) in
            l.text = ""
            l.textColor = theme.color[.primary1]
            l.font = theme.textFont(.h4, font: .bold)
            l.textAlignment = .center
            l.numberOfLines = 2
            l.lineBreakMode = .byWordWrapping
        }
        
        subtitle.style { (l) in
            l.text = ""
            l.textColor = theme.color[.primary2]
            l.font = theme.textFont(.h2, font: .light)
            l.textAlignment = .center
            l.numberOfLines = 2
            l.lineBreakMode = .byWordWrapping
        }
        
        releaseDate.style { (l) in
            l.text = ""
            l.textColor = theme.color[.primary2]
            l.font = theme.textFont(.h2, font: .light)
            l.textAlignment = .center
            l.numberOfLines = 1
            l.lineBreakMode = .byWordWrapping
        }
        
        releaseDateIcon.style { (l) in
            l.text = theme.glyph(XTGlyph.releaseDate)
            l.textColor = theme.color[.primary2]
            l.font = theme.iconFont(.h2, font: .light)
            l.textAlignment = .left
        }
        
        actionBar.style { (l) in
            l.backgroundColor = .clear
        }
        
        actionPlay.style { (l) in
            l.backgroundColor = .clear
            l.contentMode = .center
            l.setImage(UIImage.icon(code: theme.glyph(XTGlyph.play), font:theme.iconFont(.h5, font: .light), textColor: theme.color[.primary3]),for: UIControl.State.normal)
        }
        
        actionLike.style { (l) in
            l.backgroundColor = .clear
            l.contentMode = .center
            l.setImage(UIImage.icon(code: theme.glyph(XTGlyph.like), font:theme.iconFont(.h5, font: .light), textColor: theme.color[.primary3]),for: UIControl.State.normal)
            l.setImage(UIImage.icon(code: theme.glyph(XTGlyph.like), font:theme.iconFont(.h5, font: .solid), textColor: theme.color[.primary3]),for: UIControl.State.selected)
        }
        
        actionShare.style { (l) in
            l.backgroundColor = .clear
            l.contentMode = .center
            l.setImage(UIImage.icon(code: theme.glyph(XTGlyph.share), font:theme.iconFont(.h5, font: .light), textColor: theme.color[.primary3]),for: UIControl.State.normal)
        }
        
        actionNotifyMe.style { (l) in
            l.backgroundColor = .clear
            l.contentMode = .center
            l.setImage(UIImage.icon(code: theme.glyph(XTGlyph.notifyMe), font:theme.iconFont(.h5, font: .light), textColor: theme.color[.primary3]),for: UIControl.State.normal)
            l.setImage(UIImage.icon(code: theme.glyph(XTGlyph.notifyMe), font:theme.iconFont(.h5, font: .solid), textColor: theme.color[.primary3]),for: UIControl.State.selected)
        }
        
        
        actionLike.onPress { [weak self] in
            AppStore.dispatch(Actions.Shared.likeAlbum(self?.album))
        }
        
        actionNotifyMe.onPress { [weak self] in
            AppStore.dispatch(Actions.Shared.notifyMe(self?.album))
        }
        
        actionPlay.onPress { [weak self] in
            AppStore.dispatch(PlayerFlow.playAlbum(album: self?.album, from: self?.findViewController()?.navigationController))
        }
        
        
    }
    
    func placeholder() -> UIImage {
        let theme = System.theme()
        return UIImage.icon(code: theme.glyph(XTGlyph.placeHolder), font:theme.iconFont(.h6, font: .light), textColor: theme.color[.primary2])
    }
    
    func placeholderStyle(l: UIImageView) {
        l.image = placeholder()
        l.contentMode = .center
    }
    
    override func prepareForReuse() {
        coverUrl?.unsubscribe()
        coverUrl = nil
        coverLoading?.unsubscribe()
        coverLoading = nil
        cover.style(self.placeholderStyle)
        playing?.unsubscribe()
        playing = nil
    }
    
    func didUpdate(album: AlbumExtended?) {
        guard let id = album?.album.primaryId else {
            return
        }
        
        self.album = album
        
        /// new method!
        AppStore.dispatch(CoverFlow.fetchCoverUrl(albumId: id, album: album?.album.name, band: album?.band?.name))
        
        if coverUrl == nil {
            coverUrl = Prop { state in state.shared.coverUrl[id]?.url }
            coverLoading = Prop { state in state.shared.coverUrl[id]?.loading }
        }
        
        coverUrl?.didUpdate {
            if let urlString = $0, let url = URL(string: urlString) {
                self.cover.contentMode = .scaleAspectFit
                Nuke.loadImage(with: url, options: ImageLoadingOptions(placeholder: self.placeholder()), into: self.cover)
                
            }
        }
        
        coverLoading?.didUpdate { loading in
            (loading ?? false) == true ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
        }
        
        
        title.style { (l) in
            l.text = album?.album.name
        }
        
        subtitle.style { (l) in
            l.text = album?.band?.name
        }
        
        releaseDate.style { (l) in
            l.text = "\(album?.album.date?.toDate()?.toFormat("dd MMM yyyy") ?? "")"
        }
        
        if playing == nil {
            playing = Prop { state in
                return state.player.playing
                    && (state.player.album?.album.primaryId == album?.album.primaryId && album?.album.primaryId != nil)
            }
        }
        
        playing?.didUpdate { playing in
            
            self.actionPlay.style { (l) in
                let theme = System.theme()
                var glyph = theme.glyph(XTGlyph.play)
                let anySongToPlay = album?.album.songs.filter {
                    $0.streamLink.count > 0 || $0.previewLink != nil
                }.first
                
                if (anySongToPlay == nil) {
                    glyph = theme.glyph(XTGlyph.noSong)
                }else {
                    if playing {
                        glyph = theme.glyph(XTGlyph.pause)
                    }
                }
                
                l.setImage(UIImage.icon(code: glyph, font: System.theme().iconFont(.h5, font: .light), textColor: System.theme().color[.primary3]),for: UIControl.State.normal)
            }
        }
        
    }
    
    
}
