//
//  player.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/4/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import UIKit
import Stevia


class XTPlayer: UIView {

    private let cover = UIImageView()
    
    convenience init() {
        self.init(frame:CGRect.zero)

        /// 🌳
        subviews(
            cover
        )

        /// 📐
        cover.size(200).centerInContainer()
                        
        /// 🎨
        cover.style { f in
            
        }
        
    }
        
}

