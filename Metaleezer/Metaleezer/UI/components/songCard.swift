//
//  songCard.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/15/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import UIKit
import Stevia
import Nuke


class SongCard: UICollectionViewCell {
    
    let topLine = UIView()
    let title = UILabel()
    let songNumber = UILabel()
    let like = XTToggleButon()
    let more = XTActionButon()
    let play = XTActionButon()
    let playingIndicator = UILabel()
    let provider = UILabel()
    
    var song: Song?
    var band: Band?
    
    var playing: AppStateProp<Bool>? = nil
    var playingSong: AppStateProp<Bool>? = nil
    
    
    override var reuseIdentifier: String? {
        return "SongCard"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let theme = System.theme()
        
        /// 🌳
        subviews {
            topLine
            title
            songNumber
            playingIndicator
            like
            more
            provider
            play
        }
        
        /// 📐
        let m = theme.spacing(.m)
        
        topLine.height(1).fillHorizontally().top(0)
        like.size(20)
        more.size(20)
        play.size(32)
        playingIndicator.followEdges(songNumber)
        provider.followEdges(more)
        
        layout {
            m
            |-m--songNumber.width(20)--m--play--m--title--like--m--more--m-|
            m
        }
        
        /// 🎨
        backgroundColor =  theme.color[.background1]
        
        title.style { (l) in
            l.text = "Title"
            l.textColor = theme.color[.primary1]
            l.font = theme.textFont(.h2, font: .regular)
            l.textAlignment = .left
            l.numberOfLines = 2
            l.lineBreakMode = .byWordWrapping
        }
        
        songNumber.style { (l) in
            l.text = "00"
            l.textColor = theme.color[.primary2]
            l.font = theme.textFont(.h2, font: .regular)
            l.textAlignment = .left
            l.numberOfLines = 1
        }
        
        playingIndicator.style { (l) in
            l.text = theme.glyph(XTGlyph.playingIndicator)
            l.textColor = theme.color[.primary3]
            l.font = theme.iconFont(.h3, font: .solid)
            l.textAlignment = .center
        }
        
        provider.style { (l) in
            l.text = ""
            l.textColor = theme.color[.primary1]
            l.font = theme.textFont(.h2, font: .regular)
            l.textAlignment = .center
        }
        
        topLine.style { (l) in
            l.backgroundColor = theme.color[.background2]
        }
        
        like.style { (l) in
            l.backgroundColor = .clear
            l.contentMode = .center
            l.setImage(UIImage.icon(code: theme.glyph(XTGlyph.like), font:theme.iconFont(.h3, font: .regular), textColor: theme.color[.primary2]),for: UIControl.State.normal)
            l.setImage(UIImage.icon(code: theme.glyph(XTGlyph.like), font:theme.iconFont(.h3, font: .solid), textColor: theme.color[.primary2]),for: UIControl.State.selected)
        }
        
        more.style { (l) in
            l.backgroundColor = .clear
            l.contentMode = .center
            l.setImage(UIImage.icon(code: theme.glyph(XTGlyph.more), font:theme.iconFont(.h3, font: .regular), textColor: theme.color[.primary2]),for: UIControl.State.normal)
            l.setImage(UIImage.icon(code: theme.glyph(XTGlyph.more), font:theme.iconFont(.h3, font: .solid), textColor: theme.color[.primary2]),for: UIControl.State.selected)
        }
        
        play.style { (l) in
            l.backgroundColor = .clear
            l.contentMode = .center
            l.setImage(UIImage.icon(code: theme.glyph(XTGlyph.play), font:theme.iconFont(.h3, font: .regular), textColor: theme.color[.primary2]),for: UIControl.State.normal)
            l.setImage(UIImage.icon(code: theme.glyph(XTGlyph.play), font:theme.iconFont(.h3, font: .solid), textColor: theme.color[.primary2]),for: UIControl.State.selected)
        }
        
        
        like.onPress { [weak self] in
            AppStore.dispatch(Actions.Shared.likeSong(self?.song))
        }
        
        more.onPress { [weak self] in
            AppStore.dispatch(OverlayFlow.openSongActions(song: self?.song, from: self?.findViewController()?.navigationController))
        }
        
        play.onPress { [weak self] in
            AppStore.dispatch(PlayerFlow.playSong(song: self?.song, from: self?.findViewController()?.navigationController))
        }
        
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        playing?.unsubscribe()
        playing = nil
        playingSong?.unsubscribe()
        playingSong = nil
        
        let theme = System.theme()
       
        self.play.style { (l) in
            let glyph = theme.glyph(XTGlyph.play)
            l.setImage(UIImage.icon(code: glyph, font:theme.iconFont(.h3, font: .regular), textColor: theme.color[.primary2]),for: UIControl.State.normal)
        }
        
        self.playingIndicator.isHidden = true
        self.songNumber.isHidden = false
        self.provider.style { (l) in
             l.text = ""
        }
        
    }
    
    
    func didUpdate(song: Song?, band: Band?, index: Int) {
        
        let theme = System.theme()
        
        self.song = song
        self.band = band
        
        songNumber.style { (l) in
            l.text = (index + 1) < 10 ? "0\(index + 1)" : "\(index + 1)"
        }
        
        title.style { (l) in
            l.text = song?.name
        }
        
        if playing == nil {
            playing = Prop { state in
                return state.player.playing
                    && (state.player.song?.primaryId == song?.primaryId && song?.primaryId != nil)
            }
        }
        
        if playingSong == nil {
            playingSong = Prop { state in
                return (state.player.song?.primaryId == song?.primaryId && song?.primaryId != nil)
            }
        }
        
        playing?.didUpdate { playing in
            self.play.style { (l) in
                var glyph = theme.glyph(XTGlyph.play)
                if ((song?.previewLink == nil || (song?.previewLink?.isEmpty ?? true) == true) && song?.streamLink.count == 0) {
                    glyph = theme.glyph(XTGlyph.noSong)
                }else {
                    if playing {
                        glyph = theme.glyph(XTGlyph.pause)
                    }
                }
                
                l.setImage(UIImage.icon(code: glyph, font:theme.iconFont(.h3, font: .regular), textColor: theme.color[.primary2]),for: UIControl.State.normal)
            }
        }
        
        playingSong?.didUpdate { playing in
            self.playingIndicator.isHidden = !playing
            self.songNumber.isHidden = playing
            
            self.provider.style { (l) in
                if let _ = song?.streamLink[.bandcamp] {
                    l.text = "🐙"
                } else if let _ = song?.streamLink[.youtube] {
                    l.text = "🐡"
                } else if song?.streamLink[.deezer] != nil || ( song?.previewLink != nil && (song?.previewLink?.isEmpty ?? true) == false ) {
                    l.text = "🐳"
                }
                
            }
        }
    }
    
    
}

