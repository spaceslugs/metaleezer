//
//  tagCard.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/24/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import UIKit
import Stevia
import Nuke


class TagCard: UICollectionViewCell {
    
    let title = UILabel()
    
    var card: Tag?
    
    
    override var reuseIdentifier: String? {
        return "TagCard"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let theme = System.theme()
        
        /// 🌳
        subviews {
            title
        }
        
        /// 📐
        let m = theme.spacing(.m)
        let s = theme.spacing(.s)
                
        layout {
            s
            |-m-title-m-|
            s
        }
        
        /// 🎨
        backgroundColor =  .clear
        
        title.style { (l) in
            l.text = "tag"
            l.backgroundColor = theme.color[.background2]
            l.textColor = theme.color[.primary1]
            l.font = theme.textFont(.h2, font: .bold)
            l.textAlignment = .center
            l.numberOfLines = 1
            l.layer.cornerRadius = theme.borderRadius(.s)
            l.clipsToBounds = true
        }
        
    }
    
    @discardableResult
    func didUpdate(card: Tag?) ->Self {
        
        self.card = card
        
        let theme = System.theme()
        title.style { (l) in
            l.text = self.card?.tag ?? ""
            l.backgroundColor =  (self.card?.value ?? false) ? theme.color[.background2] : .clear
        }
        
        return self
        
    }
    
    
}
