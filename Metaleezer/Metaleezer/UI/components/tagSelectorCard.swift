//
//  tagSelector.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/24/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import UIKit
import Stevia


class TagSelectorCard: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
        
    var tagCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        //layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        return UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
    }()
    
    var tags: [Tag] = []
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        tagCollection.dataSource = self
        tagCollection.delegate = self
        tagCollection.register(TagCard.self, forCellWithReuseIdentifier: "TagCard")
        
        let theme = System.theme()
        
        /// 🌳
        subviews {
            tagCollection
        }
        
        /// 📐
        let s: CGFloat = CGFloat(theme.spacing(.s))
        let m: CGFloat = CGFloat(theme.spacing(.m))
        tagCollection.height(44.0)
        
        layout {
            0
            |tagCollection|
            0
        }
        
        /// 🎨
        backgroundColor = .clear
        
        tagCollection.style { (l) in
            l.backgroundColor = theme.color[.primary3]
        }
    
    
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func didUpdate(tags: [String:Bool]) ->Self {
        
        let t = tags.map { (arg0) -> Tag in
            let (key, value) = arg0
            return Tag(tag: key, value: value)
        }.sorted { (a, b) -> Bool in
            a.tag < b.tag
        }
            
        self.tags = t
        tagCollection.reloadData()
        return self
    }
    
    
    // MARK: collection view delegate
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCard", for: indexPath)
        
        if let cell = cell as? TagCard {
            cell.didUpdate(card: tags[indexPath.item])
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let card = TagCard(frame: CGRect.zero).didUpdate(card: tags[indexPath.item])
        card.setNeedsLayout()
        card.layoutIfNeeded()
        let width = card.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).width
        return CGSize(width: width +  2*CGFloat(System.theme().spacing(.m)), height: (collectionView.frame.size.height) )
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        let theme = System.theme()
        return CGFloat(theme.spacing(.m))
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(System.theme().spacing(.m))
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let theme = System.theme()
        return UIEdgeInsets(top: 0, left: CGFloat(theme.spacing(.m)), bottom: 0, right: CGFloat(theme.spacing(.m)))
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        AppStore.dispatch(Actions.Home.selectTag(tags[indexPath.item].tag))
    }
    
}


#if DEBUG
import SwiftUI

struct TagSelectorCard_Preview: PreviewProvider {
    static var previews: some View {
        TagSelectorCard()
            .didUpdate(tags: [
                        "doom": false,
                        "black": true,
                        "melodic death": true,
                        "trash": true])
            .showPreview()
    }
}
#endif
