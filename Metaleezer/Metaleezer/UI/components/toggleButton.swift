//
//  toggleButton.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/16/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import Foundation
import UIKit


class XTToggleButon: UIButton {
    private var _onPress: (() -> ())?
    
    func onPress(_ f: @escaping (() -> ()) ) {
        self.removeTarget(self, action: nil, for: .touchUpInside)
        self.addTarget(self, action: #selector(onPressAction), for: .touchUpInside)
        self._onPress = f
    }
        
    @objc func onPressAction(_ sender: UIButton) {
        self.isSelected = !self.isSelected
        self._onPress?()
    }
}
