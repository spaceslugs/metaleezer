//
//  mainPage.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/9/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import UIKit


class MainViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// configure
        let theme = System.theme()
                
        let glyphs: [XTGlyph] = [
            .home,
            .like,
            .reviews,
            .gigs,
            .profile
        ]

        let titles: [String] = [
            "Music",
            "Favourites",
            "Reviews",
            "Gigs",
            "Settings"
        ]
        
        let offset: [UIOffset] = [
            UIOffset(horizontal: 0, vertical: Device.isIphoneXOrLonger ? 6 : 0),
            UIOffset(horizontal: 0, vertical: Device.isIphoneXOrLonger ? 6 : 0),
            UIOffset(horizontal: 0, vertical: Device.isIphoneXOrLonger ? 6 : 0),
            UIOffset(horizontal: 0, vertical: Device.isIphoneXOrLonger ? 6 : 0),
            UIOffset(horizontal: 0, vertical: Device.isIphoneXOrLonger ? 7 : 0),
        ]


        tabBar.barTintColor = theme.color[.background2]
        tabBar.tintColor = theme.color[.primary3]
        
        let tabBarIcon: XTIconFont = Device.isIphoneXOrLonger ? .tabBarItemLarge : .tabBarItem
        
        for (index, item) in (tabBar.items ?? []).enumerated() {
            item.image = UIImage.icon(code: theme.glyph(glyphs[index]), font:theme.iconFont(tabBarIcon, font: .light), textColor: theme.color[.primary1])
            item.selectedImage = UIImage.icon(code: theme.glyph(glyphs[index]), font:theme.iconFont(tabBarIcon, font: .solid), textColor: theme.color[.primary1])
            item.title = titles[index]
            
            let normal: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: theme.textFont(.h1, font: .light),
                NSAttributedString.Key.foregroundColor: theme.color(.primary1)
            ]

            let selected: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.font: theme.textFont(.h1, font: .light),
                NSAttributedString.Key.foregroundColor: theme.color(.primary3)
            ]
            item.titlePositionAdjustment = offset[index]
            item.setTitleTextAttributes(normal, for: .normal)
            item.setTitleTextAttributes(selected, for: .selected)
            item.setTitleTextAttributes(selected, for: .focused)
            
        }
    }
}

