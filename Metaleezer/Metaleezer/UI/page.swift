//
//  page.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/9/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import UIKit

class Page: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let theme = System.theme()
                
        
        let barButton = UIBarButtonItem()
        barButton.image = UIImage.icon(code:theme.glyph(.back), font:theme.iconFont(.h4, font: .light), textColor: theme.color[.primary3])
        barButton.setBackgroundImage(nil, for: UIControl.State.normal, barMetrics: UIBarMetrics.default)
        barButton.title = ""
        barButton.tintColor = theme.color[.primary3]
        barButton.action = #selector(popBack)
        barButton.target = self
        
        navigationItem.hidesBackButton = true
        if ((self.navigationController?.viewControllers.count ?? 1) > 1) {
            self.navigationItem.leftBarButtonItems = [barButton]
        }else {
            self.navigationItem.leftBarButtonItems = []
        }
        
        configure()
        actions()
    }
    
    @objc func popBack(_ sender: UIButton) {
        AppStore.dispatch(RouterFlow.pop(from: self.navigationController))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        stateSubscribe()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stateUnsubscribe()
    }
    
    func configure() {
    }
    
    func actions() {
        
    }
    
    func stateSubscribe() {
    }
    
    func stateUnsubscribe() {
    }
    
}
