//
//  album.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/4/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import UIKit
import Stevia


fileprivate enum Section: Int {
    case info
    case tracks
    case otherAlbums
    case videos
    
    func cardName() -> String {
        switch self {
          case .info:
              return "AlbumInfoCard"
          case .tracks:
              return "SongCard"
          case .otherAlbums:
              return "AlbumCollectionCard"
          case .videos:
              return "AlbumVideoCollectionCard"
        }
    }
}

class AlbumPageTemplate: UIView {
    
    var activityIndicator = UIActivityIndicatorView()
    var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        //layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        return UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
    }()
    
    let loadingView = UIView()

    convenience init() {
        self.init(frame:CGRect.zero)
        
        let theme = System.theme()
        
        /// 🌳
        subviews(
            collectionView,
            loadingView,
            activityIndicator
        )
                
        /// 📐
        activityIndicator.size(32)
        activityIndicator.centerInContainer()
        
        loadingView.fillContainer()

        layout(
            0,
            |collectionView|,
            0
        )
        
        /// 🎨
        collectionView.style { (v) in
            v.backgroundColor = theme.color[.background1]
        }
        
        loadingView.style {
            $0.backgroundColor = theme.color[.background1]
        }
        
        activityIndicator.style {
            $0.style = .large
            $0.color = theme.color[.primary3]
        }
        
    }
    
}



class AlbumPage: Page, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    
    var v: AlbumPageTemplate!
    override func loadView() {
        v = AlbumPageTemplate()
        view = v
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// This is only needed for live reload
        on("INJECTION_BUNDLE_NOTIFICATION") {
            self.loadView()
            self.configure()
            self.stateUnsubscribe()
            self.stateSubscribe()
        }
    }
    
    /// MARK: state props
    var loading = Prop { state in state.pages.album.loading }
    var error = Prop { state in state.pages.album.error }
    var album = Prop { state in state.pages.album.album }
    
    var dataSource: AlbumExtended?  = nil
    
    // MARK: configure
    override func configure() {
//        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
//        UINavigationBar.appearance().shadowImage = UIImage()
//        UINavigationBar.appearance().backgroundColor = .clear
//        UINavigationBar.appearance().isTranslucent = true
        
        v.collectionView.dataSource = self
        v.collectionView.delegate = self
        v.collectionView.register(AlbumInfoCard.self, forCellWithReuseIdentifier: Section.info.cardName())
        v.collectionView.register(SongCard.self, forCellWithReuseIdentifier: Section.tracks.cardName())
    }
    
    // MARK: actions
    override func actions() {
        
    }
    
    // MARK: collection view delegate
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let s = Section(rawValue: section) ?? .info
        
        switch s {
        case .info:
            return 1
        case .tracks:
            return dataSource?.album.songs.count ?? 0
        case .otherAlbums:
            return 0
        case .videos:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let s = Section(rawValue: indexPath.section) ?? .info
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: s.cardName(), for: indexPath)
        
        if let cell = cell as? AlbumInfoCard {
            cell.didUpdate(album: dataSource)
        }

        if let cell = cell as? SongCard {
            cell.didUpdate(song: dataSource?.album.songs[indexPath.item], band: dataSource?.band, index: indexPath.item)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

         let s = Section(rawValue: indexPath.section) ?? .info

         switch s {
         case .info:
             return CGSize(width: self.view.frame.width, height: 408)
         case .tracks:
             return CGSize(width: self.view.frame.width, height: 60)
         case .otherAlbums:
             return CGSize(width: self.view.frame.width, height: 0)
         case .videos:
             return CGSize(width: self.view.frame.width, height: 0)
         }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    // MARK: stateDidUdate
    override func stateSubscribe() {
        
        
        loading.didUpdate { loading in
            loading == true ? self.v.activityIndicator.startAnimating() : self.v.activityIndicator.stopAnimating()
            self.v.loadingView.isHidden = loading == true ? false : true
        }
        
        error.didUpdate { error in
            if let e = error {
                print("⚠️ \(e)")
            }
        }
        
        album.didUpdate { (album) in
            self.dataSource = album
            self.v.collectionView.reloadData()
        }
        
    }
    
    override func stateUnsubscribe() {
        loading.unsubscribe()
        error.unsubscribe()
    }
    
}

