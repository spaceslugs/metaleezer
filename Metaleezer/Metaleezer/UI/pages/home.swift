//
//  home.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/4/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import UIKit
import Stevia

    
class HomePage: Page, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var v: HomePageTemplate!
    override func loadView() {
        v = HomePageTemplate()
        view = v
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        /// This is only needed for live reload
        on("INJECTION_BUNDLE_NOTIFICATION") {
            self.loadView()
            self.configure()
            self.stateUnsubscribe()
            self.stateSubscribe()
        }
    }
        
    /// MARK: state props
    var loading = Prop { state in state.pages.home.loading }
    var error = Prop { state in state.pages.home.error }
    var collections = Prop { state in state.pages.home.filteredCollections }
    var tags = Prop { state in state.pages.home.tags }

    var dataSource: [AlbumCollection] = []
    
    // MARK: configure
    override func configure() {
        v.collectionView.dataSource = self
        v.collectionView.delegate = self
        v.collectionView.register(AlbumCollectionCard.self, forCellWithReuseIdentifier: "AlbumCollectionCard")
    }
    
    // MARK: actions
    override func actions() {
                    
    }
    
    // MARK: collection view delegate
        
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlbumCollectionCard", for: indexPath)
        
        if let cell = cell as? AlbumCollectionCard {
            cell.didUpdate(collection: dataSource[indexPath.section])
        }
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.frame.width, height: 260)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    // MARK: stateDidUdate
    override func stateSubscribe() {
        
        
        loading.didUpdate { loading in
            loading == true ? self.v.activityIndicator.startAnimating() : self.v.activityIndicator.stopAnimating()
        }
        
        error.didUpdate { error in
            if let e = error {
                print("⚠️ \(e)")
            }
        }
        
        collections.didUpdate { (collections) in
            self.dataSource = collections
            self.v.collectionView.reloadData()
        }
        
        tags.didUpdate { (tags) in
            self.v.tags.didUpdate(tags: tags)
        }
        
    }
                 
    override func stateUnsubscribe() {
        loading.unsubscribe()
        error.unsubscribe()
    }
    
}

#if DEBUG
import SwiftUI

struct HomePage_Preview: PreviewProvider {
    static var previews: some View {
        HomePage().showPreview()
    }
}
#endif
