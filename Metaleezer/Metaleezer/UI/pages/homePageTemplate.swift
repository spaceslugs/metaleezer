//
//  HomePageTemplate.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 10/13/21.
//  Copyright © 2021 Darko Pavlovic. All rights reserved.
//

import UIKit
import Stevia


class HomePageTemplate: UIView {
         
    var activityIndicator = UIActivityIndicatorView()
    var tags = TagSelectorCard()
    var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        return UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
    }()
    
    convenience init() {
        self.init(frame:CGRect.zero)

        let theme = System.theme()
                
        /// 🌳
        subviews(
            collectionView,
            tags,
            activityIndicator
        )
        
        activityIndicator.size(32)
        activityIndicator.centerInContainer()
        
        let tagSize = 44.0
        
        tags.height(tagSize)
        tags.Top == safeAreaLayoutGuide.Bottom - 44 /// todo: tabbar height
        tags.fillHorizontally()
        
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: CGFloat(tagSize), right: 0)
        
        /// 📐
        layout(
            0,
            |collectionView|,
            0
        )

        /// 🎨
        collectionView.style { (v) in
            v.backgroundColor = theme.color[.background1]
        }

        activityIndicator.style {
            $0.style = .large
            $0.color = theme.color[.primary3]
        }
        

    }

}


#if DEBUG
import SwiftUI

struct HomePageTemplate_Preview: PreviewProvider {
    static var previews: some View {
        HomePageTemplate().showPreview()
    }
}
#endif
