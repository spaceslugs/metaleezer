//
//  splash.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/9/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import UIKit
import Stevia



class SplashPageTemplate: UIView {
    
    let activityIndicator = UIActivityIndicatorView()
    let contentView: UIView = UIView()
    let title = UILabel()
    let logo = UIImageView()

    
    convenience init() {
        self.init(frame:CGRect.zero)

        let theme = System.theme()
        
        /// 🌳
        subviews {
            contentView.subviews {
                title
            }
        }
        
        /// 📐
        contentView.fillContainer()
        title.centerInContainer()
        
        /// 🎨
        contentView.backgroundColor = System.theme().color[.background1]
        title.style { (l) in
             l.text = "Metaleezer"
             l.textColor = theme.color[.primary1]
             l.font = theme.textFont(.h5, font: .bold)
             l.textAlignment = .center
        }

    }

}

class SplashPage: Page {

    var v: SplashPageTemplate!
    override func loadView() {
        v = SplashPageTemplate()
        view = v
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        /// This is only needed for live reload
        on("INJECTION_BUNDLE_NOTIFICATION") {
            self.loadView()
            self.configure()
            self.stateUnsubscribe()
            self.stateSubscribe()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppStore.dispatch(SplashFlow.appLoading(0))
        
    }
    
    /// MARK: state props
    var loading = Prop { state in state.pages.splash.loading }
    var error = Prop { state in state.pages.splash.error }

    // MARK: configure
    override func configure() {
    }
    
    // MARK: actions
    override func actions() {
                    
    }
    
    // MARK: stateDidUdate
    override func stateSubscribe() {
        
        
        loading.didUpdate { loading in
            loading == true ? self.v.activityIndicator.startAnimating() : self.v.activityIndicator.stopAnimating()
        }
        
        error.didUpdate { error in
            if let e = error {
                print("⚠️ \(e)")
            }
        }
    }
                 
    override func stateUnsubscribe() {
        loading.unsubscribe()
        error.unsubscribe()
    }
    
}
