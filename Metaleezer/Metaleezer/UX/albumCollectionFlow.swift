//
//  albumCollectionFlow.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/15/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation


import UIKit
import ReSwift
import ReSwift_Thunk
import Then

enum AlbumCollectionFlow {

    static func open(collection: AlbumCollection?) -> Thunk<AppState> {
         return Thunk<AppState> { dispatch, getState in
                guard let _ = getState() else { return }
                guard let collection = collection else { return }

                asyncOn {
                    
                    /// 🚣 🔫 start loading flow
                    yield(.put, dispatch, Actions.AlbumList.openStart)
                    
                    /// forward data
                    yield(.put, dispatch, Actions.AlbumList.openSuccess(collection))
                    
                    /// ⏳ goto page
                    yield(.put, RouterFlow.navigateTo(.albumList, nil))

                    
                }.onError { e in
                    yield(.put, dispatch, Actions.AlbumList.openFailure(StateError(e)))
                }
        }
    }

}

