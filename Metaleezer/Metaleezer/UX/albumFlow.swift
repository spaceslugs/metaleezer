//
//  albumFlow.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/15/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import UIKit
import ReSwift
import ReSwift_Thunk
import Then
import SwiftDate

fileprivate func isNeededUpdateForStreamLinks(for albumId: EntityId) -> Bool {
    guard let album = System.music.albums[albumId], album.songs.count > 0 else {
        return false
    }
    
    guard System.music.areCompletedStreamLinks(for: albumId) else {
        var needsUpdate  = true
        /// if not found all stream links, do not check again untill 12 hours passed
        if let lastUpdateDateString = album.lastUpdateDate, let lastUpdateDate = lastUpdateDateString.toDate() {
            let nextUpdateDate =  lastUpdateDate.dateByAdding(12,.hour)
            if DateInRegion().isBeforeDate(nextUpdateDate, granularity: Calendar.Component.hour) {
                needsUpdate = false
            }
        }
        
        return needsUpdate
        
    }
    
    return false
    
}

fileprivate func isNeededUpdateForBand(for bandId: EntityId) -> Bool {
    guard let band = System.music.bands[bandId] else {
        return false
    }
    
    var needsUpdate  = true
    /// if not found all stream links, do not check again untill 12 hours passed
    if let lastUpdateDateString = band.lastUpdateDate, let lastUpdateDate = lastUpdateDateString.toDate() {
        let nextUpdateDate =  lastUpdateDate.dateByAdding(12,.hour)
        if DateInRegion().isBeforeDate(nextUpdateDate, granularity: Calendar.Component.hour) {
            needsUpdate = false
        }
    }
    
    return needsUpdate
}



enum AlbumFlow {
    
    static func open(albumExtended: AlbumExtended?, from: UINavigationController?) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            guard let _ = getState() else { return }
            guard let albumId = albumExtended?.album.primaryId,
                let album = albumExtended?.album,
                let band = albumExtended?.band else {
                    return
            }
            
            let albumByBand = AlbumByBand(album: album.name, band: band.name)
            
            asyncOn {
                
                /// 🚣 🔫 start loading flow
                yield(.put, dispatch, Actions.Album.openStart)
                
                /// ⏳ goto page
                yield(.put, RouterFlow.navigateTo(.album, from))
                
                
                /// 📜 fetch album tracks and additional info
                
                /// get song list from discovery service if not already done
                if let discovery = Providers.discovery(.metalarchives), System.music.albums[albumId]?.songs.count == 0 {
                    /// update album info
                    let update = try ..discovery.searchAlbum(id: albumId)
                    if let update = update {
                        System.music.updateAlbum(albumId: albumId, update: update)
                    }
                }
                
                /// update band info(used later for album info)
                if let discovery = Providers.discovery(.metalarchives), isNeededUpdateForBand(for: album.band) {
                    let bandUpdate = try ..discovery.searchBand(id: album.band)
                    if let bandUpdate = bandUpdate {
                        System.music.updateBand(bandId: album.band, update: bandUpdate)
                    }
                }
                
                
                if isNeededUpdateForStreamLinks(for: albumId) == true {
                    
                    let albumProviders: [ProviderId] = [.bandcamp,.deezer,.youtube]
                    
                    albumProviders.forEach {
                        guard let discovery = Providers.discovery($0) else {
                            return
                        }
                        
                        var id: EntityId? = album.id[$0]
                        var update: Album? = nil
                        
                        // try first to discover album id by provider
                        if id == nil {
                            let bandId = System.music.bandId(for: albumId, and: $0)
                            update = try? ..discovery.searchAlbum(albumByband: albumByBand, bandId: bandId)
                            id = update?.id[$0]
                            if let update = update, update.songs.count > 0 {
                                System.music.updateAlbum(albumId: albumId, update: update)
                            }
                        }
                        
                        // then discover album info
                        if let id = id, System.music.areCompletedStreamLinks(for: albumId) == false {
                            let update = try? ..discovery.searchAlbum(id: id)
                            if let update = update {
                                System.music.updateAlbum(albumId: albumId, update: update)
                            }
                        }
                        
                    }
                    
                    System.music.albums[albumId]?.lastUpdateDate = DateInRegion().toISO()
                    
                    saveData(config: persistMusicConfig, newState: System.music)
                }
                
                guard let updatedAlbum = System.music.albums[albumId] else {
                    yield(.put, dispatch, Actions.Album.openFailure(StateError(RequestError.notFound)))
                    return
                }
                
                /// forward data
                yield(.put, dispatch, Actions.Album.openSuccess(AlbumExtended(updatedAlbum)))
                
                
            }.onError { e in
                yield(.put, dispatch, Actions.Album.openFailure(StateError(e)))
            }
        }
    }
    
}
