//
//  coverFlow.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/14/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import UIKit
import ReSwift
import ReSwift_Thunk
import Then

enum CoverFlow {

    static func fetchCoverUrl(albumId: EntityId, album: String?, band: String?) -> Thunk<AppState> {
         return Thunk<AppState> { dispatch, getState in
                guard let state = getState() else { return }
            
                // check if already fetched
                guard state.shared.coverUrl[albumId]?.url == nil else {
                   return
                }
            
                asyncOn {
                    
                    /// 🚣 🔫 start loading flow
                    yield(.put, dispatch, Actions.Shared.fetchCoverUrlStart(albumId))
                    
                    var url: String? = nil
                    
                    /// chain: metalarchive-->deezer->...
                    if let discovery = Providers.discovery(.metalarchives) {
                        let update = try ..discovery.searchAlbum(id: albumId)
                        url = update?.cover
                    }
                    
                    if let url = url {
                        ///System.music.updateAlbumCover(albumId: albumId, cover: url)
                        yield(.put, dispatch, Actions.Shared.fetchCoverUrlSuccess(albumId,url))
                    }else {
                        yield(.put, dispatch, Actions.Shared.fetchCoverUrlFailure(albumId,StateError(RequestError.notFound)))
                    }
                    
                }.onError { e in
                    yield(.put, dispatch, Actions.Shared.fetchCoverUrlFailure(albumId,StateError(e)))
                }
        }
    }

}
