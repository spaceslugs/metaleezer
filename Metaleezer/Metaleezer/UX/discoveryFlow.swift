//
//  discoveryFlow.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/9/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import UIKit
import ReSwift
import ReSwift_Thunk
import Then
import SwiftDate

enum DiscoveryFlow {

    
    @discardableResult
    static func searchNewContent(_ dispatch: @escaping DispatchFunction) -> Promise<Bool> {
          return Promise { resolve, reject in
            asyncPromise {
                   /// 🚣 🔫 start
                yield(.put, dispatch, Actions.Home.searchNewReleasesStart)
                

                /// ⏳ restore music db
                System.music = restoreData(config: persistMusicConfig) ?? MusicState()
                
                /// ⏳ check if needed discovery
                var needsUpdate  = true
                if let lastUpdateDateString = System.music.lastUpdateDate, let lastUpdateDate = lastUpdateDateString.toDate() {
                    let nextUpdateDate =  lastUpdateDate.dateByAdding(12,.hour)
                    if DateInRegion().isBeforeDate(nextUpdateDate, granularity: Calendar.Component.hour) {
                        needsUpdate = false
                    }
                }
               
                var collections: [AlbumCollection]? = nil
                
                if needsUpdate {
                    /// ⏳ releases
                    var result: DiscoveryAlbumsResult = DiscoveryAlbumsResult()

                    if let discovery = Providers.discovery(.metalarchives) {
                        result = try ..discovery.searchNewReleases()
                    }
                        
                    if (result.albums.count > 0) {
                        System.music.clearDiscovery()
                        System.music.addDiscovery(data: result)
                        saveData(config: persistMusicConfig, newState: System.music)
                    }
                    
                    /// ⏳ home collections
                    collections = searchCollections()
                }

                /// ⏳  reviews
                var reviews: [Review] = []
                if let discovery = Providers.reviews(.angryMetalGuy) {
                    reviews = try ..discovery.searchReviews()
                }
                
                /// 🚣 🏁 end
                yield(.put, dispatch, Actions.Home.searchNewReleasesSuccess(reviews,collections))
                resolve(true)
           }.onError { e in
                /// 🚣 🏁 end
                yield(.put, dispatch, Actions.Home.searchNewReleasesFailure(StateError(e)))
                reject(e)
           }
        }
    }


    func searchNewContent(_ identifier: Int) -> Thunk<AppState> {
         return Thunk<AppState> { dispatch, getState in
                guard let _ = getState() else { return }
                DiscoveryFlow.searchNewContent(dispatch).then { _ in }
        }
    }


}
