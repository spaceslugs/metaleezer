//
//  overlayActions.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/16/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//



import UIKit
import ReSwift
import ReSwift_Thunk
import Then

enum OverlayFlow {
    
    static func openSongActions(song: Song?, from: UINavigationController?) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            guard let _ = getState() else { return }
        }
    }
    
    static func openAlbumActions(album: AlbumExtended?, from: UINavigationController?) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            guard let _ = getState() else { return }
        }
    }
    
    static func openBandActions(band: Band?, from: UINavigationController?) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            guard let _ = getState() else { return }
        }
    }
    
}
