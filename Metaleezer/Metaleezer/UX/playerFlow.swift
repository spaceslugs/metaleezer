//
//  playeFrlow.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/15/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import UIKit
import ReSwift
import ReSwift_Thunk
import Then




enum PlayerFlow {
    
    fileprivate static func playSong(song: Song, state: AppState) -> Promise<Bool> {
        return Promise { resolve, reject in
            asyncOn {
                
                if song.primaryId != state.player.song?.primaryId {
                    if let streamLink = song.streamLink[.bandcamp], let player = System.players[.bandcamp] {
                        let _ = try ..player.play(stream: streamLink)
                        System.currentPlayer = player
                        resolve(true)
                    } else if let streamLink = song.previewLink, let player = System.players[.bandcamp], streamLink.isEmpty == false {
                        let _ = try ..player.play(stream: streamLink)
                        System.currentPlayer = player
                        resolve(true)
                    }else {
                        reject(RequestError.serviceUnavailable)
                    }
                }else {
                    System.currentPlayer?.play() // play/pause current song
                    resolve(true)
                }
                
            }.onError { e in
                yield(.put, Actions.Player.failure(StateError(e)))
                reject(e)
            }
            
        }
        
    }
    
    static func playSong(song: Song?, from: UINavigationController?) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            guard let state = getState() else { return }
            guard let song = song else { return }
            
            asyncOn {
                let _ = try  ..playSong(song: song, state: state)
                yield(.put, Actions.Player.song(song))
            }.onError { e in
                yield(.put, Actions.Player.failure(StateError(e)))
            }
            
        }
    }
    
    static func playAlbum(album: AlbumExtended?, from: UINavigationController?) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            guard let state = getState() else { return }
            guard let album = album else { return }
            
            asyncOn {
                
                let queue = album.album.songs.filter {
                    $0.streamLink.count > 0 || ($0.previewLink != nil && $0.previewLink?.isEmpty == false)
                }
                
                if let song = queue.first {
                    let _ = try ..playSong(song: song, state: state)
                    yield(.put, Actions.Player.album(album,queue))
                }
                
            }.onError { e in
                yield(.put, Actions.Player.failure(StateError(e)))
            }
            
        }
    }
    
    static func songDidPlayToEndTime() -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            guard let state = getState() else { return }
            
            asyncOn {
                
                guard let i = state.player.queue.firstIndex(where: { state.player.song?.primaryId == $0.primaryId }) else {
                    yield(.put, Actions.Player.finishPlay)
                    return
                }
                    
                let next = i + 1
                guard next < state.player.queue.count else {
                    yield(.put, Actions.Player.finishPlay)
                    return
                }
                
                let song =  state.player.queue[next]
                
                try ..playSong(song: song, state: state)
                yield(.put, Actions.Player.nextAlbumSong(song))
                
            }.onError { e in
                yield(.put, Actions.Player.failure(StateError(e)))
            }
            
        }
    }
}
