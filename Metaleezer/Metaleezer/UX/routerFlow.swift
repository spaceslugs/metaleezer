//
//  routerFlow.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/9/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import UIKit
import ReSwift_Thunk
import Then


/// 📜
enum PageRoute: String {
    
    case boarding = "Boarding"
    case main = "MainViewController" // tabBarNavigator
    
    case home = "Home"
    case favs = "Favs"
    case reviews = "Reviews"
    case gigs = "Gigs"
    case profile = "Profile"
    
    case search = "Search"
    case album = "Album"
    case band = "Band"
    case song = "Song"
    case playlist = "Playlist"
    case review = "Review"
    case gig = "Gig"
    case lyrics = "Lyrics"
    case albumList = "AlbumList"
    case bandGigList = "BandGigList"
    
    case bandActions = "BandActions"
    case albumActions = "AlbumActions"
    case songActions = "SongActions"
    
}

/// 🎭
fileprivate func setRootPage(for route: PageRoute) {
    let vc  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: route.rawValue)
    UIApplication.shared.windows.first?.rootViewController = vc
}

/// 🎭
fileprivate func setRootPage(_ page: Page) {
    UIApplication.shared.windows.first?.rootViewController = page
}


/// 🎭
fileprivate func selectRootPage(for route: PageRoute) {
    /// todo: select tab bar item
}

/// 🎭
fileprivate func pushPage(for route: PageRoute,presenter: UINavigationController?) {
    guard let presenter = presenter else {
        return
    }
    
    let vc  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: route.rawValue)
    presenter.pushViewController(vc, animated: true)
}

/// 🎭
fileprivate func pushPage(_ page: Page, presenter: UINavigationController?) {
    guard let presenter = presenter else {
        return
    }
    
    presenter.pushViewController(page, animated: true)
}

/// 🎭
fileprivate func presentActionOverlay(_ page: Page) {
    /// todo: presentActionOverlay
}


enum RouterFlow {
    
    static func navigateTo(_ route: PageRoute, _ presentFrom: UINavigationController?) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            guard let _ = getState() else { return }
            
            switch route {
                
            case .boarding:
                setRootPage(BoardingPage())
                
            case .main:
                setRootPage(for: route)
                
            case .home:
                selectRootPage(for: route)
            case .favs:
                selectRootPage(for: route)
            case .reviews:
                selectRootPage(for: route)
            case .gigs:
                selectRootPage(for: route)
            case .profile:
                selectRootPage(for: route)
                
            case .search:
                pushPage(SearchPage(), presenter: presentFrom)
                
            case .album:
                pushPage(AlbumPage(), presenter: presentFrom)
            case .band:
                pushPage(BandPage(), presenter: presentFrom)
            case .song:
                pushPage(SongPage(), presenter: presentFrom)
            case .playlist:
                pushPage(PlaylistPage(), presenter: presentFrom)
            case .review:
                pushPage(ReviewPage(), presenter: presentFrom)
            case .gig:
                pushPage(GigPage(), presenter: presentFrom)
            case .lyrics:
                pushPage(LyricsPage(), presenter: presentFrom)
            case .albumList:
                pushPage(AlbumListPage(), presenter: presentFrom)
            case .bandGigList:
                pushPage(BandGigListPage(), presenter: presentFrom)
                
            case .albumActions:
                presentActionOverlay(AlbumActionsPage())
            case .bandActions:
                presentActionOverlay(BandActionsPage())
            case .songActions:
                presentActionOverlay(SongActionsPage())
            }
            
        }
    }
    
    static func pop(from navController: UINavigationController?) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            navController?.popViewController(animated: true)
        }
    }
    
}


