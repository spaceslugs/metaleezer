////
////  appLoadingFlow.swift
////  ACE Driver
////
////  Created by Darko Pavlovic on 4/28/20.
////  Copyright © 2020 ComputerRock. All rights reserved.
////
//
import UIKit
import ReSwift
import ReSwift_Thunk
import Then

enum SplashFlow {

    static func appLoading(_ args: Int) -> Thunk<AppState> {
         return Thunk<AppState> { dispatch, getState in
                guard let _ = getState() else { return }

                asyncOn {
                    
                    /// 🚣 🔫 start loading flow
                    yield(.put, dispatch, Actions.Splash.appLoadingStart)
                    
                    /// ⏳ search new releases
                    let _ = try ..DiscoveryFlow.searchNewContent(dispatch)
                    
                    /// ⏳ goto main tab navigator
                    yield(.put, RouterFlow.navigateTo(.main, nil))

                    /// 🚣 🏁 end loading flow
                    yield(.put, dispatch, Actions.Splash.appLoadingSuccess)
                    
                }.onError { e in

                    /// ⏳ goto main tab navigator
                    yield(.put, RouterFlow.navigateTo(.main, nil))

                    /// 🚣 🏁 end loading flow
                    yield(.put, dispatch, Actions.Splash.appLoadingFailure(StateError(e)))

                }
        }
    }

}
