//
//  Entities.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/4/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

///
typealias EntityId = String
typealias Id = [ProviderId : EntityId]

protocol Entity: PersistState {
    var id: Id { get set }
    var primaryId: EntityId? { get }
}

extension Entity {
    var primaryId: EntityId? {
        return id[.metalarchives]
            ??  id[.deezer]
            ??  id[.bandcamp]
            ??  id[.youtube]
    }
}


///
struct AlbumByBand: PersistState {
    var album: String
    var band: String
}

struct Song: Entity {
    var id: Id
    var album: EntityId
    var name: String
    
    var duration: String?
    var streamLink: [ProviderId : String] = [:]
    var lyricsLink: [ProviderId : String] = [:]
    var previewLink: String?
}


struct Album: Entity {
    var id: Id
    var band: EntityId
    var name: String

    var cover: String?
    var date: String?
    var tags: [String] = []
    var duration: String?
    var songs: [Song] = []
    
    var lastUpdateDate: String? = nil
}


struct Band: Entity {
    var id: Id
    var name: String
        
    var cover: String?
    var tags: [String] = []
    var albums: [EntityId] = []
    var country: String?
    
    var lastUpdateDate: String? = nil
    
}


struct Playlist: Entity {
    var id: Id
    
    var songs: [EntityId] = []
}

struct Review: PersistState {
    var providerId: ProviderId
    var albumByBand: AlbumByBand

    var srcLink: String?
    var text: String?
    var title: String?
    var date: String?
    
}

struct Location: PersistState {
    var longitude: String?
    var latitude: String?
}

struct  Place: PersistState {
    var name: String
    var location: Location
    var venue: String
}

enum GigType: Int, PersistState {
    case tour = 0
    case festival = 1
    case concert = 2
}

struct Gig: Entity {
    var primaryId: EntityId? {
        return id[.musicbrainz]
    }

    var id: Id
    var date: String
    var title: String

    var type: GigType?
    var cover: String?
    var text: String?
    var place: Place?
    var lineup: [String]?
    var website: String?
}


