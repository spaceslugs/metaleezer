//
//  AlbumCollection.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/13/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import Foundation
import ReSwift
import SwiftDate

/// rendered album
struct AlbumExtended: PersistState {
    var album: Album
    var band: Band?
    
    init(_ album: Album) {
        self.album = album
        self.band = System.music.bands[album.band]
    }
}

/// rendered band
struct BandExtended: PersistState {
    var band: Band
    var albums: [Album] = []
    
    init(_ band: Band) {
        self.band = band
        self.albums = band.albums.compactMap {
            System.music.albums[$0]
        }
    }
}

/// rendered collection
protocol Collection: PersistState {
    var title: String  { get set }
    func count() -> Int
}

/// rendered album collection
struct AlbumCollection: Collection {
    var title: String = "Album Collection"
    func count() -> Int { return albums.count }
    var albums: [AlbumExtended] = []
}

/// rendered band collection
struct BandCollection: Collection {
    var title: String = "Band Collection"
    func count() -> Int { return bands.count }
    var bands: [BandExtended] = []
}

extension Array  where Element == AlbumExtended {
  
    func sortAlbumsByRecent() -> [AlbumExtended] {
        return self.sorted {
            if let d1 = $0.album.date?.toDate(), let d2 = $1.album.date?.toDate() {
                return d1.isAfterDate(d2, granularity: Calendar.Component.day)
            }
            return false
        }
    }

    func sortAlbumsByClosestDate() -> [AlbumExtended] {
        return self.sorted {
            if let d1 = $0.album.date?.toDate(), let d2 = $1.album.date?.toDate() {
                return d1.isBeforeDate(d2, granularity: Calendar.Component.day)
            }
            return false
        }
    }
    
    func filterAlbums(by tags: [String : Bool]) -> [AlbumExtended] {
        return self.filter {
            guard tags.isEmpty == false ||  $0.album.tags.isEmpty else {
                return true
            }

            let albumTags = $0.album.tags.joined(separator: ",").lowercased()
               
            let selectedTags = tags.filter { tag in
                let (_ , value) = tag
                return value
            }
            
            guard selectedTags.isEmpty == false else {
                return true
            }
            
            let tagsFound = selectedTags.filter { tag in
                let (key , _) = tag
                return albumTags.contains(key.lowercased())
            }
            
            return tagsFound.count > 0 ? true : false
        }
    }

    func limitAlbums(by limit: Int) -> [AlbumExtended] {
        return Array(self.prefix(limit))
    }

}

