//
//  Tag.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/24/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import ReSwift
import SwiftDate

/// rendered tag
struct Tag: PersistState {
    var tag: String
    var value: Bool
    
    init(tag: String, value: Bool) {
        self.tag = tag
        self.value = value
    }
}
