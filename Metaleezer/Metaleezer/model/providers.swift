//
//  providers.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/5/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import Foundation
import UIKit
import Then

///
enum ProviderId: String, PersistState {
        
    case musicbrainz
    case metalarchives
    case deezer
    case youtube
    case bandcamp
    case az
    case genius
    case angryMetalGuy
 
    var me: String {
        switch self {
            
        case .musicbrainz:
            return "musicbrainz 🦞"
        case .metalarchives:
            return "metalarchives 🐙"
        case .deezer:
            return "deezer 🐳"
        case .youtube:
            return "youtube 🐡"
        case .bandcamp:
            return "bandcamp 🐟"
        case .az:
            return "az 🐠"
        case .genius:
            return "genius 🐥"
        case .angryMetalGuy:
            return "angryMetalGuy 🦅"
        }
    }
}

protocol Provider {
    var id: ProviderId { get }
    
    func log(_ activity: String)
}

extension Provider {
    func log(_ activity: String) {
        print("\(activity) \(id.me) ...")
    }
}

struct DiscoveryAlbumsResult {
    var albums: [EntityId : Album] = [:]
    var bands: [EntityId : Band] = [:]
}

struct DiscoveryAlbumResult {
    var album: Album
    var band: Band?
}

///
protocol Discovery {
    func searchNewReleases() -> Promise<DiscoveryAlbumsResult>
    func searchAlbum(id: EntityId) -> Promise<Album?>
    func searchAlbum(albumByband: AlbumByBand, bandId: EntityId?) -> Promise<Album?>
    func searchBand(id: EntityId) -> Promise<Band?>
    func searchPlaylists() -> Promise<[Playlist]>
}

protocol SongAssets {
    func searchStreamLink(id: EntityId?, songName: String?, albumByband: AlbumByBand) -> Promise<String?>
}

protocol Lyrics {
    func searchLyrics(id: EntityId?, songName: String?, albumByband: AlbumByBand) -> Promise<String?>
}

protocol Reviews {
    func searchReviews() -> Promise<[Review]>
}

protocol Gigs {
    func searchEvents() -> Promise<[Gig]>
}

protocol Webzines {
    func searchWebzines()
}

protocol Themes {
    func searchThemes()
}



/// Providers
enum Providers {
    static var providers: [ProviderId : Provider] = [
        .musicbrainz : MusicBrainz(),
        .metalarchives : MetalArchives(),
        .angryMetalGuy : AngryMetalGuy(),
        .bandcamp : Bandcamp(),
        .deezer : Deezer(),
        .youtube : Youtube()
    ]

    static func discovery(_ id: ProviderId) -> Discovery? {
        guard let p = providers[id] as? Discovery else {
            return nil
        }
        return p
    }

    static func lyrics(_ id: ProviderId) -> Lyrics? {
        guard let p = providers[id] as? Lyrics else {
            return nil
        }
        return p
    }

    static func reviews(_ id: ProviderId) -> Reviews? {
        guard let p = providers[id] as? Reviews else {
            return nil
        }
        return p
    }

}
