//
//  stateError.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/9/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import Foundation

protocol StorableError: PersistState {
    var code: Int { get set }
    var message: String { get set }
}

struct FieldError: PersistState {
    var code = 0
    var field = ""
    var message = ""
}


struct StateError: StorableError {
    var code = 0
    var message = ""
    var errors: [FieldError] = []
    
    init(_ code: Int = 0, _ message: String = "", _ errors: [FieldError] = []) {
        self.code = code
        self.message = message
        self.errors = errors
    }
    
    init(_ error: Error) {
        let e = error as NSError
        self.code = e.code
        self.message = e.localizedDescription
        // todo: extract from NSError field errrors(userInfo?)
        
    }

    init(_ error: RequestError) {
        self.code = error.httpCode
        self.message = RequestError.reason(forHTTPCode: code)
    }
}
