//
//  system.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/5/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import Then

///
protocol Player {
    func play(stream: String) -> Promise<Bool>
    func play()
}


/// System
enum System {
    
    static var defaultTheme = "Dark"

    static var music: MusicState = MusicState()
    
    static var players: [ProviderId : Player] = [
        .deezer : DeezerPlayer(),
        .youtube : YoutubePlayer(),
        .bandcamp : BandcampPlayer(),
    ]
    
    static var currentPlayer: Player? = nil
    
    static var themes: [String : Theme] = [
        System.defaultTheme : AppTheme()
    ]

    static func theme(_ theme: String = defaultTheme) ->Theme {
        return System.themes[theme] ?? System.themes[System.defaultTheme]!
    }
    
}

