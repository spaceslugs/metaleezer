//
//  Theme.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/5/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics


enum XTTextFontName: String  {
    case regular
    case light
    case bold
}

enum XTIconFontName: String {
    case regular
    case solid
    case light
}


enum XTTextFont: String  {
    case h1
    case h2
    case h3
    case h4
    case h5
}

enum XTIconFont: String {
    case h1
    case h2
    case h3
    case h4
    case h5
    case h6
    case tabBarItem
    case tabBarItemLarge
}


enum XTColor: String {
    case background1 // black
    case background2 // dark gray

    case primary1  // white
    case primary2  // gray
    case primary3  // red
    
    case secondary1 // orange
    case secondary2 // green
    case secondary3 // blue
}

enum XTSpacing {
    case s
    case m
    case l
    case xl
}

enum XTBorderRadius {
    case s
    case m
    case l
}

enum XTGlyph {
    /// tabbar
    case home
    case like
    case reviews
    case gigs
    case profile

    /// nav bar
    case search
    case back

    /// player
    case play
    case pause
    case next
    case lyrics

    /// pages
    case loading
    case share
    case more
    case notifyMe
    case releaseDate
    case album
    case albumCollection
    case band
    case songCredits
    case playlist
    case review
    case reviewSite
    case gig
    case lineup
    case place
    case date
    case resetSearch
    case searchPlaceholder
    case addToPlaylist
    case removeFromPlaylist
    case placeHolder
    case noSong
    case playingIndicator
}


protocol Theme {
    var name: String { get set }
    
    var textFontName: [XTTextFontName: String] { get set }
    var iconFontName: [XTIconFontName: String] { get set }
    var textFontSize: [XTTextFont: CGFloat] { get set }
    var iconFontSize: [XTIconFont: CGFloat] { get set }
    var spacing: [XTSpacing: Int] { get set }
    var color: [XTColor: UIColor] { get set }
    var borderRadius: [XTBorderRadius: CGFloat] { get set }
    var glyph: [XTGlyph: String] { get set }
    
    
    func textFont(_ key: XTTextFont, font: XTTextFontName) ->UIFont
    func iconFont(_ key: XTIconFont, font: XTIconFontName) -> UIFont
    func textFontSize(_ value: XTTextFont) -> CGFloat
    func iconFontSize(_ value: XTIconFont) -> CGFloat
    func spacing(_ value: XTSpacing) -> Int
    func color(_ value: XTColor) -> UIColor
    func borderRadius(_ value: XTBorderRadius) -> CGFloat
    func glyph(_ value: XTGlyph) -> String

}



fileprivate enum ThemeAssets {
    static var textFonts: [XTTextFont: UIFont?] = [:]
    static var iconFonts: [XTIconFont: UIFont?] = [:]
}

extension Theme {
    
    func textFontSize(_ value: XTTextFont) -> CGFloat {
        return textFontSize[value] ?? 16
    }
    
    func iconFontSize(_ value: XTIconFont) -> CGFloat {
        return iconFontSize[value] ?? 16
    }
    
    func spacing(_ value: XTSpacing) -> Int {
        spacing[value] ?? 16
    }
    
    func color(_ value: XTColor) -> UIColor {
        color[value] ?? .yellow
    }
    
    func borderRadius(_ value: XTBorderRadius) -> CGFloat {
        borderRadius[value] ?? 5
    }
    
    func glyph(_ value: XTGlyph) -> String {
        return glyph[value] ?? "\u{f54c}"
    }

    func iconFont(_ key: XTIconFont, font: XTIconFontName) ->UIFont {
        return UIFont(name: iconFontName[font] ?? "FontAwesome5ProRegular", size: iconFontSize[key] ?? 16) ??
        UIFont.systemFont(ofSize: 16)
    }

    func textFont(_ key: XTTextFont, font: XTTextFontName) ->UIFont {
        return UIFont(name: textFontName[font] ?? "Overpass-Regular", size: textFontSize[key] ?? 16) ?? UIFont.systemFont(ofSize: 16)
    }

}


struct AppTheme: Theme {
    
    var name = "Dark"
    

    var textFontName: [XTTextFontName: String] = [
        .regular : "Overpass-Regular",
        .light : "Overpass-Light",
        .bold : "Overpass-Bold",
    ]

    var iconFontName: [XTIconFontName: String] = [
        .regular : "FontAwesome5ProRegular",
        .light : "FontAwesome5ProLight",
        .solid : "FontAwesome5ProSolid",
    ]

    var textFontSize: [XTTextFont: CGFloat] = [
        .h1 : 11,
        .h2 : 14,
        .h3 : 18,
        .h4 : 22,
        .h5 : 28,
    ]

    var iconFontSize: [XTIconFont: CGFloat] = [
        .h1 : 11,
        .h2 : 14,
        .h3 : 18,
        .h4 : 22,
        .h5 : 28,
        .h6 : 36,
        .tabBarItem : 24,
        .tabBarItemLarge : 28
    ]


    var spacing: [XTSpacing: Int] = [
        .s : 8,
        .m : 16,
        .l : 24,
        .xl : 32
    ]

    var borderRadius: [XTBorderRadius: CGFloat] = [
       .s : 4,
       .m : 8,
       .l : 16
    ]

    var color: [XTColor : UIColor] = [
        .background1 : .black,
        .background2 : UIColor(red: 0.05, green: 0.05, blue: 0.08, alpha: 1),
            
        .primary1 : .white,
        .primary2 : UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1),
        .primary3 : .red,

        .secondary1 : .systemOrange,
        .secondary2 : .systemGreen,
        .secondary3 : .systemBlue,
        
            
    ]
    
    var glyph: [XTGlyph: String] = [
        .home : "\u{f001}",
        .like : "\u{f004}",
        .reviews : "\u{f6b7}",
        .gigs : "\u{f569}",
        .profile : "\u{f54c}",
        
        .albumCollection : "\u{f8a0}",
        .album : "\u{f8d9}",
        .share : "\u{f1e0}",
        .play : "\u{f04b}",
        .pause : "\u{f04c}",
        .more : "\u{f141}",
        .notifyMe : "\u{f0f3}",
        .releaseDate : "\u{f133}",
        .back : "\u{f053}",
        .placeHolder : "\u{f001}",
        .noSong : "\u{f656}",
        .playingIndicator : "\u{f025}"
        
    ]
    
}


