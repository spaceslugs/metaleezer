//
//  bancamp.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/4/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import Then

class PlayerView: UIView {
    
    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    var player: AVPlayer? {
        get {
            return playerLayer.player
        }
        set {
            playerLayer.player = newValue
        }
    }
    
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }
    
    private var playerItemContext = 0
    
    // Keep the reference and use it to observe the loading status.
    private var playerItem: AVPlayerItem?
    
    private var _onStatus: ((Bool) -> ())?
    
    
    private func setUpAsset(with url: URL, completion: ((_ asset: AVAsset) -> Void)?) {
        let asset = AVAsset(url: url)
        asset.loadValuesAsynchronously(forKeys: ["playable"]) {
            var error: NSError? = nil
            let status = asset.statusOfValue(forKey: "playable", error: &error)
            switch status {
            case .loaded:
                completion?(asset)
            case .failed:
                print(".failed")
                self._onStatus?(false)
            case .cancelled:
                print(".cancelled")
                self._onStatus?(false)
            default:
                print("default")
            }
        }
    }
    
    private func setUpPlayerItem(with asset: AVAsset) {
        
        playerItem?.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status))
        
        playerItem = AVPlayerItem(asset: asset)
        playerItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.old, .new], context: &playerItemContext)
        
        if let item = playerItem {
            DispatchQueue.main.async { [weak self] in
                self?.player?.replaceCurrentItem(with: item)
            }
        }
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        // Only handle observations for the playerItemContext
        guard context == &playerItemContext else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            let status: AVPlayerItem.Status
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItem.Status(rawValue: statusNumber.intValue) ?? .unknown
            } else {
                status = .unknown
            }
            // Switch over status value
            switch status {
            case .readyToPlay:
                print(".readyToPlay")
                self._onStatus?(true)
                player?.play()
            case .failed:
                print(".failed")
                self._onStatus?(false)
            case .unknown:
                print(".unknown")
            @unknown default:
                print("@unknown default")
            }
        }
    }
    
    func play(with url: URL, _ onStatus: @escaping ((Bool) -> ()) ) {
        self._onStatus = onStatus
        setUpAsset(with: url) { [weak self] (asset: AVAsset) in
            self?.setUpPlayerItem(with: asset)
        }
    }
    
    deinit {
        playerItem?.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status))
        print("deinit of PlayerView")
    }
}




class BandcampPlayer: Player {
    
    private var playerView: PlayerView?
    private var playerRateObserver: NSKeyValueObservation?
    
    private func setAudioSessionActive(_ isActive: Bool) {
        try? AVAudioSession.sharedInstance().setCategory(.playback, mode: .moviePlayback)
        try? AVAudioSession.sharedInstance().setActive(isActive)
    }
    
    private let videoURL = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
    
    
    init() {
        playerView = PlayerView()
        playerView?.player = AVPlayer()
        playerView?.player?.usesExternalPlaybackWhileExternalScreenIsActive = true
        playerRateObserver = playerView?.player?.observe(
            \.rate,
            options: [.new],
            changeHandler: { (player, observedChange) in
                let isPlaying = player.rate > 0.0
                yield(.put, Actions.Player.playing(isPlaying))
        })
        
        NotificationCenter.default.addObserver(self, selector: #selector(audioDidPlayToEndTime),
                                                         name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                         object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        playerRateObserver = nil
        playerView?.player?.usesExternalPlaybackWhileExternalScreenIsActive = false
        playerView?.player = nil
    }
    
    @objc func audioDidPlayToEndTime(notification: Notification) {
        yield(.put, PlayerFlow.songDidPlayToEndTime())
    }
    
    
    func play(stream: String) -> Promise<Bool> {
        
        return Promise { (resolve, reject) in
            DispatchQueue.main.async {
                print("🐟 play bandcamp ...")
                
                guard let url = URL(string: stream) else {
                    reject(RequestError.badRequest)
                    return
                }
                
                self.setAudioSessionActive(true)
                
                self.playerView?.play(with: url) { (status) in
                    status ? resolve(true) : reject(RequestError.serviceUnavailable)
                }
            }
        }
        
    }
    
    
    func play() {
        
        DispatchQueue.main.async {
            guard let player = self.playerView?.player else { return }
            
            let isPlaying = player.rate > 0.0
            
            if isPlaying {
                player.pause()
            } else {
                player.play()
            }
        }
                    
    }
    
}
