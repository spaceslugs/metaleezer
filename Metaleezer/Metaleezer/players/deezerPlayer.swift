//
//  deezer.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/4/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import Then

class DeezerPlayer: Player {
    
    func play(stream: String) -> Promise<Bool> {
        return Promise {  resolve, reject in
            print("🐳 play deezer ...")
            resolve(false)
        }
    }
    
    func play() {
        
    }
}
