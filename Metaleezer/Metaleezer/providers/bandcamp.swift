//
//  bandcamp.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/4/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import Then
import SwiftSoup
import JavaScriptCore
import Fuse


extension String {
    func matches(pattern: String) -> [String] {
        do {
            let regEx = try NSRegularExpression(pattern: pattern, options: [])
            let nsString = self as NSString
            let matches = regEx.matches(in: self, options: [], range: NSMakeRange(0, nsString.length))
            let output = matches.map {nsString.substring(with: $0.range)}
            return output
        } catch {
            return []
        }
        
    }
}

class Bandcamp: NSObject, Provider, Discovery, Lyrics
{
    
    var id: ProviderId = ProviderId.bandcamp
    
    fileprivate func albumData(html: String, albumName: String) ->Album? {
        var songs: [Song] = []
        var title: String? = nil
        var id: String? = nil
        
        let songLinkPattern = "(https://t4.bcbits.com/stream/[0-9a-fA-F]+/mp3-128/[0-9]+\\?p=0&amp;ts=[0-9]+&amp;t=[0-9a-fA-F]+&amp;token=[0-9a-zA-Z_]+)"
        let songLinks = html.matches(pattern: songLinkPattern)
                
    
        var document: Document = Document.init("")
        do {
            document = try SwiftSoup.parse(html)
            let scriptElement = try document.select("script").first(where: { (item) -> Bool in
                let type = try item.attr("type")
                return type == "application/ld+json" ? true : false
            })
            
            guard let script  = scriptElement else {
                print("\(ProviderId.bandcamp.me) 🍌: parsing script data")
                return nil
            }
            
            let content = try script.html()
            guard let data  = content.data(using: .utf8) else {
                print("\(ProviderId.bandcamp.me) 🍌: parsing data")
                return nil
            }
            
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else {
                print("\(ProviderId.bandcamp.me) 🍌: parsing json")
                return nil
            }
                            
            id = json["@id"] as? String
            title = json["name"] as? String
            
            guard let albumId = id else {
                print("\(ProviderId.bandcamp.me) 🍌: album title not found")
                return nil
            }
            
            guard let track = json["track"] as? [String : Any],
                  let tracks = track["itemListElement"] as? [[String : Any]] else {
                print("\(ProviderId.bandcamp.me) 🍌: parsing track json")
                return nil
            }
            
            songs = tracks.compactMap {
                let item = $0["item"] as? [String: Any]
                
                let title = item?["name"] as? String
                let id = item?["@id"] as? String
                
                guard let additionalProperty = item?["additionalProperty"] as? [[String:Any]] else {
                    return nil
                }
                
                let trackIdElement = additionalProperty.first(where: { (item) -> Bool  in
                    guard let name = item["name"] as? String else {
                        return false
                    }
                    return name == "track_id"
                })
                
                
                guard let track_id = trackIdElement?["value"] as? Int else {
                    return nil
                }
                
                let link = songLinks.first {
                    $0.contains("\(track_id)")
                }
                
                ///let link = songIdToLink["\(track_id)"]
                              
                guard let link = link else {
                    return nil
                }
                                
                return Song(
                    id: [.bandcamp: id ?? link],
                    album: albumId,
                    name: title ?? "unknown",
                    duration: nil,
                    streamLink: [.bandcamp : link]
                )
            }
            
            
        } catch {
            print("\(ProviderId.bandcamp.me) 🍌: parsing album(\(albumName) error -> \(error)")
            return nil
        }
               
        
        guard let albumTitle = title, let albumId = id else {
            print("\(ProviderId.bandcamp.me) 🍌: album title not found")
            return nil
        }
             
        
        let a = albumTitle.lowercased().trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let b = albumName.lowercased().trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        let result = Fuse().search(a, in: b)
        print("🍌: search <\(a)> in <\(b)> -> \(result?.score ?? 1.0)")
        guard (result?.score ?? 1.0) < 0.1  else {
            print("\(ProviderId.bandcamp.me) 🍌: album title score low(\(result?.score ?? 1.0)")
            return nil
        }
        
        
        let album = Album(
            id: [.bandcamp : albumId],
            band: "",
            name: "",
            cover: nil,
            date: nil,
            tags: [],
            duration: nil,
            songs:songs)
        
        return album
        
    }

    /// MARK: Discovery
    func searchNewReleases() -> Promise<DiscoveryAlbumsResult> {
        return Promise { resolve, reject in
            let result = DiscoveryAlbumsResult()
            self.log("🥁... searchNewReleases ")
            
            resolve(result)
        }
    }
    
    func searchAlbum(id: EntityId) -> Promise<Album?> {
        return Promise { resolve, reject in
            let result: Album? = nil
            self.log("🥁... searchAlbum \(id) ")
            
            
            resolve(result)
        }
    }
    
    func searchAlbum(albumByband: AlbumByBand, bandId: EntityId?) -> Promise<Album?> {
        return Promise { resolve, reject in
            
            self.log("🥁... searchAlbum \(albumByband.album) by \(albumByband.band) ")
            
            asyncOn {
                
                guard let bandIdLink = bandId else {
                    reject(RequestError.badRequest)
                    return
                }
                
                let c = URLComponents(string: "\(bandIdLink)/releases")
                guard let url = c?.url else {
                    reject(RequestError.badRequest)
                    return
                }
             
                do {
                    
                    var html = try String.init(contentsOf: url)
                    
                    /// try first if album data is directly on band page(no discography div or it is last album)
                    let albumData = self.albumData(html: html, albumName: albumByband.album)
                    
                    if let album = albumData, album.songs.count > 0 {
                        resolve(album)
                        return
                    }
                    
                    /// then discography
                    
                    let c = URLComponents(string: "\(bandIdLink)/music")
                    guard let url = c?.url else {
                        reject(RequestError.badRequest)
                        return
                    }
                    
                    html = try String.init(contentsOf: url)
                    var document: Document = Document.init("")
                    document = try SwiftSoup.parse(html)
                    guard let albums: Elements = try? document.select("a") else {
                        reject(RequestError.badRequest)
                        return
                    }
                    
                    let albumFound: [String] = albums.array().compactMap {
                        guard let href = try? $0.attr("href"), href.hasPrefix("/album") else {
                            return nil
                        }
                                                
                        guard let title = try? $0.select("p").text().trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) else {
                            return nil
                        }
                        
                        /// some more advanced fuzzy search
                        let result = Fuse().search(title, in: albumByband.album)
                        print("🍌: search <\(title)> in <\(albumByband.album)> -> \(result?.score ?? 1.0)")
                        guard (result?.score ?? 1.0) < 0.1  else {
                            return nil
                        }

//                        guard albumByband.album == title else {
//                            return nil
//                        }
                        
                        return "\(bandIdLink)/\(href)"
                        
                    }
                    
                    /// todo: if no matching title try matching album songs
                    
                    guard let albumId = albumFound.first else {
                        reject(RequestError.badRequest)
                        return
                    }
                                        
                    
                    let album = Album(
                        id: [.bandcamp : albumId],
                        band: "",
                        name: "",
                        cover: nil,
                        date: nil,
                        tags: [],
                        duration: nil,
                        songs: [])
                    
                    resolve(album)
                    
                } catch let error {
                    reject(error)
                }
                
            }.onError { (e) in
                reject(e)
            }
            
        }
        
    }
    
    func searchBand(id: EntityId) -> Promise<Band?> {
        return Promise { resolve, reject in
            let result: Band? = nil
            self.log("🥁... searchBand")
            
            resolve(result)
        }
    }
    
    func searchPlaylists() -> Promise<[Playlist]> {
        return Promise { resolve, reject in
            let result: [Playlist] = []
            self.log("🥁... searchPlaylists")
            
            resolve(result)
        }
    }
    
    /// MARK: Lyrics
    func searchLyrics(id: EntityId?, songName: String?, albumByband: AlbumByBand) -> Promise<String?> {
        return Promise { resolve, reject in
            let result: String? = nil
            ///
            self.log("🥁... searchLyrics for song \(songName ?? "") in ")
            
            resolve(result)
        }
    }
}


/*
 
 https://wilderun.bandcamp.com/track/scentless-core-fading#lyrics
 
 <div class="tralbumData lyricsText">If the air touches skin with its grace
 <br>
 If the gift of love gives an embrace
 <br>
 If a blade finds its mark and blood is drawn
 <br>
 Still i think i’ll be far, i’ll be gone
 <br>
 
 <br>
 If the breath of today does not sway
 <br>
 Tomorrow’s memory just may
 <br>
 It is all i can hope for as of now
 <br>
 For the scent of today is too foul
 <br>
 
 <br>
 Is this meant as as twisted cure?
 <br>
 Protection from all the hells endured
 <br>
 I’ll take anything, as long as it stays
 <br>
 Within sight always
 </div>
 
 https://bandcamp.com/search?q=sleep%20at%20the%20edge%20of%20the%20world&page=1
 
 https://bandcamp.com/search?q=Veil%20of%20Imagination%20wilderun
 
 <li class="searchresult album">
 <a class="artcont" href="https://wilderun.bandcamp.com/album/veil-of-imagination?from=search&amp;search_item_id=3278252367&amp;search_item_type=a&amp;search_match_part=%3F&amp;search_page_id=1192820138&amp;search_page_no=1&amp;search_rank=1&amp;search_sig=9fdd64904311f0ae0442b086a5dce639">
 <div class="art">
 
 <img src="https://f4.bcbits.com/img/a0899408251_7.jpg">
 
 
 
 
 </div>
 </a
 ><div class="result-info">
 <div class="itemtype">
 ALBUM
 
 </div>
 
 <div class="heading">
 <a href="https://wilderun.bandcamp.com/album/veil-of-imagination?from=search&amp;search_item_id=3278252367&amp;search_item_type=a&amp;search_match_part=%3F&amp;search_page_id=1192820138&amp;search_page_no=1&amp;search_rank=1&amp;search_sig=9fdd64904311f0ae0442b086a5dce639">
 Veil of Imagination
 
 </a>
 </div>
 
 
 <div class="subhead">
 by Wilderun
 </div>
 
 <div class="length">
 8 tracks, 66 minutes
 </div>
 <div class="released">
 released 01 November 2019
 </div>
 
 <div class="itemurl">
 <a href="https://wilderun.bandcamp.com/album/veil-of-imagination?from=search&amp;search_item_id=3278252367&amp;search_item_type=a&amp;search_match_part=%3F&amp;search_page_id=1192820138&amp;search_page_no=1&amp;search_rank=1&amp;search_sig=9fdd64904311f0ae0442b086a5dce639">https://wilderun.bandcamp.com/album/veil-of-imagination</a>
 </div>
 
 
 <div class="tags">
 tags:
 
 Melodic Death Metal,
 
 Massachusetts,
 
 Symphonic Metal,
 
 Folk metal,
 
 Devin Townsend,
 
 United States,
 
 Edge of Sanity,
 
 Boston,
 
 Folk Metal,
 
 Opeth,
 
 Turisas,
 
 Metal,
 
 Wintersun,
 
 Progressive Metal
 
 
 </div>
 
 </div>
 </li>
 
 
 https://t4.bcbits.com/stream/624c5572fab6cad2129db2545113d8f2/mp3-128/2550385969?p=0&ts=1591243629&t=c0fffae61c788ffe1276808f6d2fd2d1fb7a02be&token=1591243629_0404d1a0df072f21276fe6f0d152f257920035e8
 
 <iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=2136785864/size=small/bgcol=ffffff/linkcol=de270f/transparent=true/" seamless><a href="http://vanagandrmusic.bandcamp.com/album/born-of-sorcery">Born of Sorcery by Vanagandr</a></iframe>
 
 https://t4.bcbits.com/stream/624c5572fab6cad2129db2545113d8f2/mp3-128/2550385969?p=0&ts=1591243629&t=c0fffae61c788ffe1276808f6d2fd2d1fb7a02be&token=1591243629_0404d1a0df072f21276fe6f0d152f257920035e8
 
 
 <audio preload="auto" controls="controls" autoplay="autoplay">
    <source src="https://t4.bcbits.com/stream/624c5572fab6cad2129db2545113d8f2/mp3-128/2550385969?p=0&ts=1591243629&t=c0fffae61c788ffe1276808f6d2fd2d1fb7a02be&token=1591243629_0404d1a0df072f21276fe6f0d152f257920035e8" type='audio/mpeg'>
 </audio>
 
 
 <html><body><video controls autoplay="" src="https://t4.bcbits.com/stream/624c5572fab6cad2129db2545113d8f2/mp3-128/2550385969?p=0&amp;ts=1591243629&amp;t=c0fffae61c788ffe1276808f6d2fd2d1fb7a02be&amp;token=1591243629_0404d1a0df072f21276fe6f0d152f257920035e8" type="audio/mpeg" class="media-document audio mac"></video></body></html>
 
 
 
 var TralbumData = {
 // For the curious:
 // http://bandcamp.com/help/audio_basics#steal
 // http://bandcamp.com/terms_of_use
 current: {"featured_track_id":3739945088,"title":"Valley of Deception","purchase_url":null,"new_desc_format":1,"about":"A journey to the Valley of Deception.","minimum_price":0.0,"is_set_price":null,"purchase_title":null,"credits":null,"require_email":null,"release_date":"22 May 2020 00:00:00 GMT","audit":0,"auto_repriced":null,"require_email_0":null,"private":null,"mod_date":"23 Jun 2020 01:01:43 GMT","id":2170491513,"upc":null,"art_id":3037547133,"killed":null,"band_id":1422338127,"artist":null,"new_date":"21 May 2020 10:19:27 GMT","type":"album","set_price":7.0,"publish_date":"22 May 2020 16:12:38 GMT","download_desc_id":null,"selling_band_id":1422338127,"minimum_price_nonzero":7.0,"download_pref":2},
 is_preorder: null,
 album_is_preorder: null,
 album_release_date: "22 May 2020 00:00:00 GMT",
 preorder_count: null,
 hasAudio: true,
 art_id: 3037547133,
 trackinfo: [{"encoding_error":null,"is_downloadable":true,"license_type":1,"title":"Requiem","track_license_id":null,"video_mobile_url":null,"album_preorder":false,"track_id":3739945088,"encoding_pending":null,"video_id":null,"has_free_download":null,"encodings_id":3143084164,"video_poster_url":null,"lyrics":null,"duration":660.896,"unreleased_track":false,"play_count":null,"is_draft":false,"free_album_download":false,"video_caption":null,"streaming":1,"title_link":"/track/requiem","private":null,"is_capped":null,"sizeof_lyrics":0,"video_featured":null,"has_lyrics":false,"id":3739945088,"video_source_type":null,"alt_link":null,"has_info":false,"file":{"mp3-128":"https://t4.bcbits.com/stream/edfd62ade053ece68eb10defb8ce0d27/mp3-128/3739945088?p=0&ts=1593022522&t=0249afa17b1c4778f2c35750e5e4690623d50d70&token=1593022522_3e370f6b4f743cc216107294f2746ea8f982a794"},"video_source_id":null,"track_num":1},{"encoding_error":null,"is_downloadable":true,"license_type":1,"title":"Extinct","track_license_id":null,"video_mobile_url":null,"album_preorder":false,"track_id":608514140,"encoding_pending":null,"video_id":null,"has_free_download":null,"encodings_id":1687465506,"video_poster_url":null,"lyrics":null,"duration":349.254,"unreleased_track":false,"play_count":null,"is_draft":false,"free_album_download":false,"video_caption":null,"streaming":1,"title_link":"/track/extinct","private":null,"is_capped":null,"sizeof_lyrics":0,"video_featured":null,"has_lyrics":false,"id":608514140,"video_source_type":null,"alt_link":null,"has_info":false,"file":{"mp3-128":"https://t4.bcbits.com/stream/2ba1f0b5637ab7ff64f256f093964fda/mp3-128/608514140?p=0&ts=1593022522&t=827c7db71e6460d4f32ef1ba9a282f433e9cbc9d&token=1593022522_05cd38ddf25b583016419ea192683befe8821542"},"video_source_id":null,"track_num":2},{"encoding_error":null,"is_downloadable":true,"license_type":1,"title":"The End","track_license_id":null,"video_mobile_url":null,"album_preorder":false,"track_id":1787908697,"encoding_pending":null,"video_id":null,"has_free_download":null,"encodings_id":3804800017,"video_poster_url":null,"lyrics":null,"duration":351.0,"unreleased_track":false,"play_count":null,"is_draft":false,"free_album_download":false,"video_caption":null,"streaming":1,"title_link":"/track/the-end","private":null,"is_capped":null,"sizeof_lyrics":0,"video_featured":null,"has_lyrics":false,"id":1787908697,"video_source_type":null,"alt_link":null,"has_info":false,"file":{"mp3-128":"https://t4.bcbits.com/stream/ed2a2b45f1dfe3e49bce8ff78a9de733/mp3-128/1787908697?p=0&ts=1593022522&t=963410642cf78e83bd2bcce0c17069ae10b30e17&token=1593022522_c1fa543d6eb6b7afdc190a6d9e952c99fde20f87"},"video_source_id":null,"track_num":3},{"encoding_error":null,"is_downloadable":true,"license_type":1,"title":"Valley of Deception","track_license_id":null,"video_mobile_url":null,"album_preorder":false,"track_id":3371185755,"encoding_pending":null,"video_id":null,"has_free_download":null,"encodings_id":801233749,"video_poster_url":null,"lyrics":null,"duration":373.771,"unreleased_track":false,"play_count":null,"is_draft":false,"free_album_download":false,"video_caption":null,"streaming":1,"title_link":"/track/valley-of-deception","private":null,"is_capped":null,"sizeof_lyrics":0,"video_featured":null,"has_lyrics":false,"id":3371185755,"video_source_type":null,"alt_link":null,"has_info":false,"file":{"mp3-128":"https://t4.bcbits.com/stream/3a9d17b3dae6153fa30abe22dc7c36ea/mp3-128/3371185755?p=0&ts=1593022522&t=f18072f38c25e02fe62e87c5dc0e72c9ddbffeac&token=1593022522_9abd4fb4799c98fba3dcea489944448ce2f6574b"},"video_source_id":null,"track_num":4},{"encoding_error":null,"is_downloadable":true,"license_type":1,"title":"Sealed Gravestone","track_license_id":null,"video_mobile_url":null,"album_preorder":false,"track_id":3469720354,"encoding_pending":null,"video_id":null,"has_free_download":null,"encodings_id":3637774519,"video_poster_url":null,"lyrics":null,"duration":440.597,"unreleased_track":false,"play_count":null,"is_draft":false,"free_album_download":false,"video_caption":null,"streaming":1,"title_link":"/track/sealed-gravestone","private":null,"is_capped":null,"sizeof_lyrics":0,"video_featured":null,"has_lyrics":false,"id":3469720354,"video_source_type":null,"alt_link":null,"has_info":false,"file":{"mp3-128":"https://t4.bcbits.com/stream/c08fdaedb4c98eea60c08a59fbffc807/mp3-128/3469720354?p=0&ts=1593022522&t=140fcdc4dc1472b3a5300a6d494ee5dba447a781&token=1593022522_ff3527c52a9d72b0143be27f225c0979951cfa30"},"video_source_id":null,"track_num":5},{"encoding_error":null,"is_downloadable":true,"license_type":1,"title":"Black Fog","track_license_id":null,"video_mobile_url":null,"album_preorder":false,"track_id":1723445862,"encoding_pending":null,"video_id":null,"has_free_download":null,"encodings_id":2694956059,"video_poster_url":null,"lyrics":null,"duration":401.143,"unreleased_track":false,"play_count":null,"is_draft":false,"free_album_download":false,"video_caption":null,"streaming":1,"title_link":"/track/black-fog","private":null,"is_capped":null,"sizeof_lyrics":0,"video_featured":null,"has_lyrics":false,"id":1723445862,"video_source_type":null,"alt_link":null,"has_info":false,"file":{"mp3-128":"https://t4.bcbits.com/stream/e97fba5deae404d6b43e50aa653f4bf3/mp3-128/1723445862?p=0&ts=1593022522&t=06c484489325455b9a318682e824127e2217c091&token=1593022522_019d53df7203dd573104ff02419f003f54a999d3"},"video_source_id":null,"track_num":6}],
 playing_from: "album page",
 featured_track_id: 3739945088,
 initial_track_num: null,
 packages: null,
 url: "http://dimdust.bandcamp.com" + "/album/valley-of-deception",
 defaultPrice: 7.0,
 freeDownloadPage: "https://bandcamp.com/download?id=2170491513&ts=1592936122.4007279468&tsig=132b2e5b242a50cb13fb549c13feda43&type=album",
 FREE: 1,
 PAID: 2,
 artist: "Dim Dust",
 item_type: "album", // xxx: note - don't internationalize this variable
 id: 2170491513,
 last_subscription_item: null,
 has_discounts: null,
 is_bonus: null,
 play_cap_data: null,
 client_id_sig: null,
 is_purchased: null,
 items_purchased: null,
 is_private_stream: null,
 is_band_member: null,
 licensed_version_ids: null,
 package_associated_license_id: null,
 tralbum_collect_info: {"show_wishlist_tooltip":false,"show_collect":true}
 };
 
 var TralbumData = {
     // For the curious:
     // http://bandcamp.com/help/audio_basics#steal
     // http://bandcamp.com/terms_of_use
     current: {"type":"album","purchase_title":null,"release_date":"26 Jun 2020 00:00:00 GMT","require_email":null,"auto_repriced":null,"require_email_0":null,"audit":0,"upc":null,"art_id":1695196298,"band_id":1822215651,"artist":null,"mod_date":"24 May 2020 00:04:54 GMT","killed":null,"selling_band_id":1822215651,"set_price":10.0,"download_pref":2,"publish_date":"24 May 2020 00:04:54 GMT","new_date":"23 May 2020 23:53:43 GMT","download_desc_id":null,"minimum_price_nonzero":10.0,"private":null,"title":"The Wrath","about":null,"id":2487537615,"featured_track_id":160667034,"purchase_url":null,"new_desc_format":1,"minimum_price":7.0,"credits":null,"is_set_price":1},
     is_preorder: null,
     album_is_preorder: null,
     album_release_date: "26 Jun 2020 00:00:00 GMT",
     preorder_count: null,
     hasAudio: true,
     art_id: 1695196298,
     trackinfo: [{"video_mobile_url":null,"album_preorder":false,"file":{"mp3-128":"https://t4.bcbits.com/stream/e088c9edf0f0530c2552c2f4b4e770cc/mp3-128/160667034?p=0&ts=1593247041&t=a8fb4606eed4bf7ad832439e3b4bc2e5fcf660fe&token=1593247041_460ca748f79b2d875372ef7f2f383583ece3899b"},"encoding_pending":null,"lyrics":null,"has_free_download":null,"streaming":1,"video_poster_url":null,"unreleased_track":false,"play_count":null,"is_draft":false,"free_album_download":false,"video_caption":null,"title_link":"/track/06-vulcano","is_capped":null,"sizeof_lyrics":0,"video_featured":null,"has_lyrics":false,"video_source_type":null,"private":null,"title":"06 - Vulcano","alt_link":null,"has_info":false,"track_id":160667034,"track_license_id":null,"video_source_id":null,"track_num":1,"encodings_id":985891828,"id":160667034,"encoding_error":null,"video_id":null,"duration":342.323,"is_downloadable":true,"license_type":1}],
     playing_from: "album page",
     featured_track_id: 160667034,
     initial_track_num: null,
     packages: [{"release_date":null,"download_has_audio":true,"album_art_id":null,"currency":"EUR","price":15.0,"subscriber_only_published":false,"label":null,"download_art_id":1695196298,"download_url":"https://velkhanos.bandcamp.com/track/06-vulcano","arts":[{"width":1600,"file_name":"321974238","image_id":20172512,"index":0,"id":2597506441,"height":1600}],"type_id":11,"album_artist":null,"upc":null,"desc_pt1":"Artwork: The Wrath\n\nT-Shirt Fruit Of The Loom","url":"/merch/the-wrath-t-shirt","quantity_sold":null,"download_artist":"Velkhanos","band_id":1822215651,"options_title":"T-Shirt size","tax_rate":null,"subscriber_only":null,"desc_pt2":null,"associated_license_id":null,"album_release_date":null,"album_private":null,"edition_size":null,"new_date":"24 May 2020 00:18:46 GMT","options":[{"quantity":null,"quantity_sold":null,"title":"Man S","origins":[{"quantity":null,"package_id":706606565,"quantity_sold":0,"option_id":3983906019,"quantity_available":null,"id":3607600}],"quantity_available":null,"sku":"","index":0,"id":3983906019},{"quantity":null,"quantity_sold":null,"title":"Man M","origins":[{"quantity":null,"package_id":706606565,"quantity_sold":0,"option_id":3743661622,"quantity_available":null,"id":3607600}],"quantity_available":null,"sku":"","index":1,"id":3743661622},{"quantity":null,"quantity_sold":null,"title":"Man L","origins":[{"quantity":null,"package_id":706606565,"quantity_sold":0,"option_id":2680744265,"quantity_available":null,"id":3607600}],"quantity_available":null,"sku":"","index":2,"id":2680744265},{"quantity":null,"quantity_sold":null,"title":"Man XL","origins":[{"quantity":null,"package_id":706606565,"quantity_sold":0,"option_id":2983112774,"quantity_available":null,"id":3607600}],"quantity_available":null,"sku":"","index":3,"id":2983112774},{"quantity":null,"quantity_sold":null,"title":"Man XXL","origins":[{"quantity":null,"package_id":706606565,"quantity_sold":0,"option_id":2868782552,"quantity_available":null,"id":3607600}],"quantity_available":null,"sku":"","index":4,"id":2868782552},{"quantity":null,"quantity_sold":null,"title":"Women S","origins":[{"quantity":null,"package_id":706606565,"quantity_sold":0,"option_id":422129353,"quantity_available":null,"id":3607600}],"quantity_available":null,"sku":"","index":5,"id":422129353},{"quantity":null,"quantity_sold":null,"title":"Women M","origins":[{"quantity":null,"package_id":706606565,"quantity_sold":0,"option_id":2653768521,"quantity_available":null,"id":3607600}],"quantity_available":null,"sku":"","index":6,"id":2653768521},{"quantity":null,"quantity_sold":null,"title":"Women L","origins":[{"quantity":null,"package_id":706606565,"quantity_sold":0,"option_id":4062087685,"quantity_available":null,"id":3607600}],"quantity_available":null,"sku":"","index":7,"id":4062087685},{"quantity":null,"quantity_sold":null,"title":"Women XL","origins":[{"quantity":null,"package_id":706606565,"quantity_sold":0,"option_id":2128309542,"quantity_available":null,"id":3607600}],"quantity_available":null,"sku":"","index":8,"id":2128309542},{"quantity":null,"quantity_sold":null,"title":"Women XXL","origins":[{"quantity":null,"package_id":706606565,"quantity_sold":0,"option_id":2421385064,"quantity_available":null,"id":3607600}],"quantity_available":null,"sku":"","index":9,"id":2421385064}],"selling_band_id":1822215651,"featured_date":null,"country":null,"album_upc":null,"private":null,"grid_index":0,"title":"\"The Wrath\" T-Shirt","certified_seller":null,"album_id":null,"quantity_limits":null,"quantity_available":null,"origins":[{"quantity":null,"package_id":706606565,"quantity_sold":null,"option_id":0,"quantity_available":null,"id":3607600}],"description":"Artwork: The Wrath\n\nT-Shirt Fruit Of The Loom","type_name":"T-Shirt/Apparel","album_title":null,"fulfillment_days":7,"download_title":"06 - Vulcano","download_id":160667034,"shipping_exception_mode":null,"sku":"","new_desc_format":1,"id":706606565,"album_publish_date":null,"quantity_warning":null,"download_type":"t","album_art":null,"is_set_price":1}],
     url: "http://velkhanos.bandcamp.com" + "/album/the-wrath",
     defaultPrice: 7.0,
     freeDownloadPage: null,
     FREE: 1,
     PAID: 2,
     artist: "Velkhanos",
     item_type: "album", // xxx: note - don't internationalize this variable
     id: 2487537615,
     last_subscription_item: null,
     has_discounts: null,
     is_bonus: null,
     play_cap_data: {"streaming_limits_enabled":true,"streaming_limit":3},
     client_id_sig: "scSpGnK9XuU9M7upPRtl9oml3d0=",
     is_purchased: null,
     items_purchased: null,
     is_private_stream: null,
     is_band_member: null,
     licensed_version_ids: null,
     package_associated_license_id: null,
     tralbum_collect_info: {"show_wishlist_tooltip":false,"show_collect":true}
 };

 */



