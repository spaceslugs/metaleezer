//
//  testAlbum.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/18/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation

struct TestAlbum {
    let album: AlbumByBand = AlbumByBand(album: "Winter's Gate", band: "Insomnium")
    let youtube = (browseId: "MPREb_omX8i9q9cNg",key: "AIzaSyC9XL3ZjWddXya6X74dJoCTL-WEYFDNX30")
}
 
let useTestAlbum = false
