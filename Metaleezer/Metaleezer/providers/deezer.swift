//
//  deezer.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/4/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import Then
import SwiftDate
import SwiftSoup

/*
 API Explorer
 https://developers.deezer.com/api/explorer?url=search%3Fq%3Deminem
 
 https://api.deezer.com/search?q=artist:"kalmah" album:"For The Revolution" 10 total
 https://api.deezer.com/search/album?q=artist:"kalmah" album:"For The Revolution" 1 total
 */


class Deezer: Provider, Discovery {
    var id: ProviderId = ProviderId.deezer
    
    /// MARK: Discovery
    func searchNewReleases() -> Promise<DiscoveryAlbumsResult> {
        return Promise { resolve, reject in
            asyncOn {
                let result = DiscoveryAlbumsResult()
                self.log("🥁... searchNewReleases ")
                resolve(result)
            }.onError { e in
                reject(e)
            }
        }
    }
    
    ///
    /// https://api.deezer.com/album/5286451
    ///
    func searchAlbum(id: EntityId) -> Promise<Album?> {
        
        // MARK: - DeezerAlbumResponse
        struct DeezerAlbumResponse: Codable {
            let id: Int?
            let title, upc: String?
            let link, share, cover: String?
            let coverSmall, coverMedium, coverBig, coverXl: String?
            let genreID: Int?
            let genres: Genres?
            let label: String?
            let nbTracks, duration, fans, rating: Int?
            let releaseDate, recordType: String?
            let available: Bool?
            let tracklist: String?
            let explicitLyrics: Bool?
            let explicitContentLyrics, explicitContentCover: Int?
            let contributors: [Contributor]?
            let artist: Artist?
            let type: String?
            let tracks: Tracks?

            enum CodingKeys: String, CodingKey {
                case id, title, upc, link, share, cover
                case coverSmall = "cover_small"
                case coverMedium = "cover_medium"
                case coverBig = "cover_big"
                case coverXl = "cover_xl"
                case genreID = "genre_id"
                case genres, label
                case nbTracks = "nb_tracks"
                case duration, fans, rating
                case releaseDate = "release_date"
                case recordType = "record_type"
                case available, tracklist
                case explicitLyrics = "explicit_lyrics"
                case explicitContentLyrics = "explicit_content_lyrics"
                case explicitContentCover = "explicit_content_cover"
                case contributors, artist, type, tracks
            }
        }

        // MARK: - Artist
        struct Artist: Codable {
            let id: Int?
            let name: String?
            let picture: String?
            let pictureSmall, pictureMedium, pictureBig, pictureXl: String?
            let tracklist: String?
            let type: String?

            enum CodingKeys: String, CodingKey {
                case id, name, picture
                case pictureSmall = "picture_small"
                case pictureMedium = "picture_medium"
                case pictureBig = "picture_big"
                case pictureXl = "picture_xl"
                case tracklist, type
            }
        }

        // MARK: - Contributor
        struct Contributor: Codable {
            let id: Int?
            let name: String?
            let link, share, picture: String?
            let pictureSmall, pictureMedium, pictureBig, pictureXl: String?
            let radio: Bool?
            let tracklist: String?
            let type: String?
            let role: String?

            enum CodingKeys: String, CodingKey {
                case id, name, link, share, picture
                case pictureSmall = "picture_small"
                case pictureMedium = "picture_medium"
                case pictureBig = "picture_big"
                case pictureXl = "picture_xl"
                case radio, tracklist, type, role
            }
        }

        // MARK: - Genres
        struct Genres: Codable {
            let data: [ArtistElement]?
        }

        // MARK: - ArtistElement
        struct ArtistElement: Codable {
            let id: Int?
            let name: String?
            let picture: String?
            let type: String?
            let tracklist: String?
        }

        // MARK: - Tracks
        struct Tracks: Codable {
            let data: [TracksDatum]?
        }

        // MARK: - TracksDatum
        struct TracksDatum: Codable {
            let id: Int?
            let readable: Bool?
            let title, titleShort, titleVersion: String?
            let link: String?
            let duration, rank: Int?
            let explicitLyrics: Bool?
            let explicitContentLyrics, explicitContentCover: Int?
            let preview: String?
            let artist: ArtistElement?
            let type: String?

            enum CodingKeys: String, CodingKey {
                case id, readable, title
                case titleShort = "title_short"
                case titleVersion = "title_version"
                case link, duration, rank
                case explicitLyrics = "explicit_lyrics"
                case explicitContentLyrics = "explicit_content_lyrics"
                case explicitContentCover = "explicit_content_cover"
                case preview, artist, type
            }
        }

        
        func task(session: URLSession, url: URL, completionHandler: @escaping (DeezerAlbumResponse?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            return session.codableTask(with: url, completionHandler: completionHandler)
        }
        
        return Promise { resolve, reject in
            asyncOn {
                self.log("🥁... searchAlbum \(id) ")
                
                let albumId = id
                
                guard let url = URL(string: "https://api.deezer.com/album/\(albumId)") else {
                    reject(RequestError.badRequest)
                    return
                }
                
                task(session: URLSession.shared, url: url) { (data, response, error) in
                    guard let deezerAlbum = data else {
                        reject(error ?? RequestError.badRequest)
                        return
                    }
                    
                    guard let id = deezerAlbum.id,
                        let title = deezerAlbum.title,
                        let bandId = deezerAlbum.artist?.id else {
                            reject(error ?? RequestError.notFound)
                            return
                    }
                    
                    self.log("🥁✅ -> \(deezerAlbum.title ?? "") by \(deezerAlbum.artist?.name ?? "") ")
                    
                    let songs = deezerAlbum.tracks?.data?.compactMap {
                        Song(
                            id: [.deezer : "\($0.id ?? 0)"],
                            album: "\(id)",
                            name: $0.title ?? "",
                            duration: "\($0.duration ?? 0)",
                            streamLink: [:],
                            lyricsLink: [:],
                            previewLink: ($0.preview?.isEmpty ?? true) ? nil : $0.preview)
                    } ?? []
                    
                    
                    ///let releaseDate = deezerAlbum.releaseDate?.toDate("yyyy-MM-dd")?.toISO()
                    
                    let result = Album(
                        id: [.deezer : "\(id)"],
                        band: "\(bandId)",
                        name: title,
                        cover: nil,
                        date: nil,
                        tags: [],
                        duration: "\(deezerAlbum.duration ?? 0)",
                        songs: songs
                    )
                    
                    resolve(result)
                }.resume()
                
            }.onError { (e) in
                reject(e)
            }
        }
    }
    
    
    ///
    ///  https://api.deezer.com/search/album?q=artist:"kalmah" album:"For The Revolution"
    ///
    func searchAlbum(albumByband: AlbumByBand, bandId: EntityId?) -> Promise<Album?> {
        
        // MARK: - SearchAlbumResponse
        struct SearchAlbumResponse: Codable {
            let data: [SearchAlbum]?
            let total: Int?
        }
        
        // MARK: - SearchAlbum
        struct SearchAlbum: Codable {
            let id: Int?
            let title: String?
            let link, cover: String?
            let cover_small, cover_medium, cover_big, cover_xl: String?
            let genre_id, nb_tracks: Int?
            let record_type: String?
            let tracklist: String?
            let explicit_lyrics: Bool?
            let artist: SearchArtist?
            let type: String?
        }
        
        // MARK: - SearchArtist
        struct SearchArtist: Codable {
            let id: Int?
            let name: String?
            let link, picture: String?
            let picture_small, picture_medium, picture_big, picture_xl: String?
            let tracklist: String?
            let type: String?
        }
        
        func task(session: URLSession, url: URL, completionHandler: @escaping (SearchAlbumResponse?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            return session.codableTask(with: url, completionHandler: completionHandler)
        }
        
        
        return Promise { resolve, reject in
            asyncOn {
                self.log("🥁... searchAlbum \(albumByband.album) by \(albumByband.band) ")
                
                var album = albumByband.album
                var band = albumByband.band

                if useTestAlbum {
                    album = TestAlbum().album.album
                    band = TestAlbum().album.band
                }
                
                var c = URLComponents(string: "https://api.deezer.com/search/album?")
                c?.queryItems?.append(URLQueryItem(name: "q", value: "artist:'\(band)' album:'\(album)'" ))
                
                guard let url = c?.url else {
                    reject(RequestError.badRequest)
                    return
                }
                                
                task(session: URLSession.shared, url: url) { (data, response, error) in
                    guard let data = data?.data else {
                        reject(error ?? RequestError.badRequest)
                        return
                    }
                    
                    let searchAlbum = data.filter {
                        $0.type == "album"
                            && $0.artist?.name == band
                            && $0.title == album
                    }.first
                    
                    guard let deezerAlbum = searchAlbum,
                        let id = deezerAlbum.id,
                        let bandId = deezerAlbum.artist?.id else {
                            reject(error ?? RequestError.notFound)
                            return
                    }
                    
                    self.log("🥁✅ -> \(albumByband.album) by \(albumByband.band) ")
                    
                    let result = Album(
                        id: [.deezer : "\(id)"],
                        band: "\(bandId)",
                        name: album,
                        cover: searchAlbum?.cover_medium)
                    
                    resolve(result)
                }.resume()
                
            }.onError { e in
                reject(e)
            }
        }
        
    }
    
    func searchBand(id: EntityId) -> Promise<Band?> {
        return Promise { resolve, reject in
            let result: Band? = nil
            self.log("🥁... searchBand")
            
            resolve(result)
        }
    }
    
    func searchPlaylists() -> Promise<[Playlist]> {
        return Promise { resolve, reject in
            let result: [Playlist] = []
            self.log("🥁... searchPlaylists")
            
            resolve(result)
        }
    }
    
    /// MARK: Lyrics
    func searchLyrics(id: EntityId?, songName: String?, albumByband: AlbumByBand) -> Promise<String?> {
        return Promise { resolve, reject in
            let result: String? = nil
            ///
            self.log("🥁... searchLyrics for song \(songName ?? "") in ")
            
            resolve(result)
        }
    }
}


