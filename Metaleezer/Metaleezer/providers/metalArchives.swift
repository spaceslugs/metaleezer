//
//  metalArchives.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/4/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import Then
import SwiftDate
import SwiftSoup

/*
 DATE CHEAT SHEET
 https://devhints.io/datetime
 
 "https://www.metal-archives.com/search/ajax-advanced/searching/albums?bandName=&releaseType%5B%5D=1&releaseTitle=&releaseYearFrom=2020&releaseMonthFrom=06&releaseYearTo=2020&releaseMonthTo=06&country=&location=&releaseLabelName=&releaseCatalogNumber=&releaseIdentifiers=&releaseRecordingInfo=&releaseDescription=&releaseNotes=&genre=-black%20-blackened&sEcho=1&iColumns=3&sColumns=&iDisplayStart=0&iDisplayLength=200&sNames=%2C%2C"
 
 
 https://www.metal-archives.com/search/ajax-advanced/searching/albums/?bandName=&releaseTitle=&releaseYearFrom=2020&releaseMonthFrom=06&releaseYearTo=2020&releaseMonthTo=06&country=&location=&releaseLabelName=&releaseCatalogNumber=&releaseIdentifiers=&releaseRecordingInfo=&releaseDescription=&releaseNotes=&genre=&releaseType[]=1&sEcho=1&iColumns=3&sColumns=&iDisplayStart=0&iDisplayLength=200&mDataProp_0=0&mDataProp_1=1&mDataProp_2=2&_=1592983360089
 
 album cover
 <div class="album_img">
 <a class="image" id="cover" title="Ensiferum - Two Paths" href="https://www.metal-archives.com/images/6/6/2/0/662053.jpg?5601"><img src="https://www.metal-archives.com/images/6/6/2/0/662053.jpg?5601" title="Click to zoom" alt="Ensiferum - Two Paths" border="0" /></a>
 </div>
 
 
 */

// MARK: - Welcome
struct Welcome: Codable {
    let error: String?
    let iTotalRecords, iTotalDisplayRecords, sEcho: Int?
    let aaData: [[String]]?
}


// MARK: - URLSession response handlers
extension URLSession {
    func welcomeTask(with url: URL, completionHandler: @escaping (Welcome?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}


class MetalArchives: Provider, Discovery, Lyrics {
    var id: ProviderId = ProviderId.metalarchives
     
    var result = DiscoveryAlbumsResult()
    var iDisplayStart = 0
    let iDisplayLength = 200
    var iTotalRecords = 10000
    var iTotalRead = 0
    
    func searchNewReleasesPage(iDisplayStart: Int, iDisplayLength: Int) -> Promise<Int> {
        return Promise { resolve, reject in
           
            let releaseDateFrom = DateInRegion() - 15.days
            let releaseDateTo = DateInRegion() + 15.days

            let releaseMonthFrom = releaseDateFrom.month > 9 ? "\(releaseDateFrom.month)" : "0\(releaseDateFrom.month)"
            let releaseMonthTo = releaseDateTo.month > 9 ? "\(releaseDateTo.month)" : "0\(releaseDateTo.month)"
            let releaseYearFrom = "\(releaseDateFrom.year)"
            let releaseYearTo = "\(releaseDateTo.year)"

            
            var c = URLComponents(string: "https://www.metal-archives.com/search/ajax-advanced/searching/albums?")
            c?.queryItems?.append(URLQueryItem(name: "bandName", value: "" ))
            c?.queryItems?.append(URLQueryItem(name: "releaseType[]", value: "1" ))
            c?.queryItems?.append(URLQueryItem(name: "releaseTitle=", value: "" ))
            c?.queryItems?.append(URLQueryItem(name: "releaseYearFrom", value: releaseYearFrom ))
            c?.queryItems?.append(URLQueryItem(name: "releaseMonthFrom", value: releaseMonthFrom ))
            c?.queryItems?.append(URLQueryItem(name: "releaseYearTo", value: releaseYearTo ))
            c?.queryItems?.append(URLQueryItem(name: "releaseMonthTo", value: releaseMonthTo ))
            c?.queryItems?.append(URLQueryItem(name: "country", value: "" ))
            c?.queryItems?.append(URLQueryItem(name: "location", value: "" ))
            c?.queryItems?.append(URLQueryItem(name: "releaseLabelName", value: "" ))
            c?.queryItems?.append(URLQueryItem(name: "releaseCatalogNumber", value: "" ))
            c?.queryItems?.append(URLQueryItem(name: "releaseIdentifiers", value: "" ))
            c?.queryItems?.append(URLQueryItem(name: "releaseRecordingInfo", value: "" ))
            c?.queryItems?.append(URLQueryItem(name: "releaseDescription", value: "" ))
            c?.queryItems?.append(URLQueryItem(name: "releaseNotes", value: "" ))
            c?.queryItems?.append(URLQueryItem(name: "genre", value: "+-blackeneddd" ))
            c?.queryItems?.append(URLQueryItem(name: "sEcho", value: "1" ))
            c?.queryItems?.append(URLQueryItem(name: "iColumns", value: "3" ))
            c?.queryItems?.append(URLQueryItem(name: "iDisplayStart", value: "\(iDisplayStart)" ))
            c?.queryItems?.append(URLQueryItem(name: "iDisplayLength", value: "\(iDisplayLength)" ))
            
            guard let url = c?.url else {
                reject(RequestError.badRequest)
                return
            }
            
            let _ = URLSession.shared.welcomeTask(with: url) { welcome, response, error in
                guard let welcome = welcome else {
                    reject(error ?? RequestError.badRequest)
                    return
                }
                
                var document: Document = Document.init("")
                welcome.aaData?.forEach {
                    let band = $0[0]
                    let album = $0[1]
                    let genre = $0[2]
                    let date = $0[3]
                    
                    print("🎸 \(band),\(album),\(date)")
                    
                    do {
                        document = try SwiftSoup.parse(band)
                        let a1 = try document.select("a")
                        let bandId = try a1.attr("href")
                        let bandName = try a1.text()
                        ///print("🎸 bandId: \(bandId)  bandName: \(bandName)")
                        
                        document = try SwiftSoup.parse(album)
                        let a2 = try document.select("a")
                        let albumId = try a2.attr("href")
                        let albumName = try a2.text()
                        ///print("🎸 albumId: \(albumId) albumName: \(albumName)")
                        
                        var albumDate: String? = nil
                        
                        if let startOfSentence = date.firstIndex(of: "<"), let endOfSentence = date.firstIndex(of: ">") {
                            if let startIndex = date.index(startOfSentence, offsetBy: 4, limitedBy: date.endIndex),
                                let endIndex = date.index(endOfSentence, offsetBy: -4, limitedBy: date.startIndex) {
                                
                                let dateString = date[startIndex...endIndex].trimmingCharacters(in: .whitespacesAndNewlines)
                                albumDate = dateString.toDate("yyyy-MM-dd")?.toISO()
                                ///print("⏰\(albumDate)")
                            }
                        }
                        
                        self.result.bands[bandId] = Band(id:  [.metalarchives : bandId], name: bandName)
                        self.result.albums[albumId] = Album(id: [.metalarchives :  albumId], band: bandId, name: albumName, cover: nil, date: albumDate, tags: [genre] )
                        
                    } catch let error {
                        reject(error)
                        return
                    }
                }
                
                print("⏰", #function, #line)
                
                self.iTotalRead += welcome.aaData?.count ?? 0
                self.iTotalRecords = welcome.iTotalRecords ?? 0
                
                print("🎸 iTotalRead: \(self.iTotalRead) of: \(self.iTotalRecords)")
                resolve(self.iTotalRead)
                
            }.resume()
            
        }
    }
    
    /// MARK: Discovery
    func searchNewReleases() -> Promise<DiscoveryAlbumsResult> {
        
        return Promise { resolve, reject in
                       
            self.log("🥁... searchNewReleases ")
            
            asyncOn {
                
                self.iTotalRead = 0
                self.iTotalRecords = 10000
                
                while self.iTotalRead < self.iTotalRecords  {
                            
                    let _ = try ..self.searchNewReleasesPage(iDisplayStart: self.iDisplayStart,iDisplayLength: self.iDisplayLength)
                                        
                    if self.iTotalRead < self.iTotalRecords {
                        self.iDisplayStart += self.iDisplayLength
                    }else {
                        resolve(self.result)
                    }
                    
                }
                
            }.onError { e in
                /// 🚣 🏁 end
                reject(e)
            }
            
        }
    }
    
    
    
    func searchAlbum(id: EntityId) -> Promise<Album?> {
        return Promise { resolve, reject in
            var result: Album? = nil
            self.log("🥁... searchAlbum \(id) ")
            
            struct Track {
                var id: String?
                var name: String?
                var duration: String?
            }
            
            asyncOn {
                
                guard let url = URL(string: id) else {
                    reject(RequestError.badRequest)
                    return
                }
                
                let html = try String.init(contentsOf: url)
                var document: Document = Document.init("")
                
                do {
                    document = try SwiftSoup.parse(html)
                    let div = try document.select("div.album_img")
                    let cover = try div.select("#cover").attr("href")
                    print("🎸 cover: \(cover)")
                    
                    let table = try document.getElementsByClass("display table_lyrics")
                    let tr: Elements = try table.select("tr:not(.displayNone)")
                    let tracks: [Track] = try tr.array().filter {
                        let id = try? $0.select("td").first()?.select("a").attr("name")
                        return (id?.isEmpty ?? true) ? false : true
                    }.map {
                        var track = Track()
                        let td = try $0.select("td").array()
                        if let a = td.first {
                            track.id = try? a.select("a").attr("name")
                        }
                        if (td.count >= 2) {
                            track.name = try? td[1].text()
                        }
                        if (td.count >= 3) {
                            track.duration = try? td[2].text()
                        }
                        
                        return track
                    }
                    
                    let songs = tracks.compactMap {
                        Song(
                            id: [.metalarchives : $0.id ?? ""],
                            album: "\(id)",
                            name: $0.name ?? "",
                            duration: $0.duration,
                            streamLink: [:],
                            lyricsLink: [:]
                        )
                    }
                    
                    
                    result = Album(id: [.metalarchives :  id], band: "", name: "", cover: cover, songs: songs)
                    
                } catch let error {
                    reject(error)
                    return
                }
                
                resolve(result)
                
            }.onError { (e) in
                reject(e)
            }
            
            
        }
    }
    
    func searchAlbum(albumByband: AlbumByBand, bandId: EntityId?) -> Promise<Album?> {
        return Promise { resolve, reject in
            let result: Album? = nil
            self.log("🥁... searchAlbum \(albumByband.album) by \(albumByband.band) ")
            
            resolve(result)
        }
        
    }
    
    func searchBand(id: EntityId) -> Promise<Band?> {
        return Promise { resolve, reject in
            
            self.log("🥁... searchBand")
            
            
            
            let supportedProviders: [String:ProviderId] = [
                "Bandcamp" : .bandcamp,
                "Deezer" : .deezer,
                "Youtube" : .youtube
            ]
            
            var providers: [ProviderId:EntityId] = [:]
            
            asyncOn {
                /// discography tab is default
                ///
                ///
                /// fetch links tab
                /// https://www.metal-archives.com/link/ajax-list/type/band/id/3540451512
                
                guard let bandId = id.components(separatedBy: "/").last else {
                    reject(RequestError.badRequest)
                    return
                }
                
                let c = URLComponents(string: "https://www.metal-archives.com/link/ajax-list/type/band/id/\(bandId)")
                guard let url = c?.url else {
                    reject(RequestError.badRequest)
                    return
                }
                
                
                let html = try String.init(contentsOf: url)
                var document: Document = Document.init("")
                
                do {
                    document = try SwiftSoup.parse(html)
                    let table = try document.select("#linksTableOfficial")
                    let tr: Elements = try table.select("tbody").select("tr")
                    tr.array().forEach {
                        let linkObject = try? $0.select("td").first()?.select("a")
                        let providerId = try? linkObject?.text()
                        let link = try? linkObject?.attr("href")
                        if let providerId = providerId, let p = supportedProviders[providerId] {
                            providers[p] = link
                        }
                    }
                    
                    /// todo: discography, country, tags, cover
                    
                    resolve(Band(id: providers, name: "", cover: nil, tags: [], albums: [], country: nil))
                    
                } catch let error {
                    reject(error)
                }
                
            }.onError { (e) in
                reject(e)
            }
            
        }
    }
    
    func searchPlaylists() -> Promise<[Playlist]> {
        return Promise { resolve, reject in
            let result: [Playlist] = []
            self.log("🥁... searchPlaylists")
            
            resolve(result)
        }
    }
    
    /// MARK: Lyrics
    func searchLyrics(id: EntityId?, songName: String?, albumByband: AlbumByBand) -> Promise<String?> {
        return Promise { resolve, reject in
            let result: String? = nil
            ///
            self.log("🥁... searchLyrics for song \(songName ?? "") in ")
            
            resolve(result)
        }
    }
}
