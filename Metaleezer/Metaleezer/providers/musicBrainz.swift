//
//  musicBrainz.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/4/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import Then

/*
 "http://musicbrainz.org/ws/2/release-group/?query=\"From Afar\"%20AND%20type:album%20AND%20artistname=Ensiferum"
 
 const mbApi = new MusicbrainzApi({
 appName: 'my-app',
 appVersion: '0.1.0',
 appContactInfo: 'user@mail.org' // Or URL to application home page
 });
 
 const config = {
 // MusicBrainz bot account username & password (optional)
 botAccount: {
 username: 'myUserName_bot',
 password: 'myPassword'
 },
 
 // API base URL, default: 'https://musicbrainz.org' (optional)
 baseUrl: 'https://musicbrainz.org',
 
 appName: 'my-app',
 appVersion: '0.1.0',
 
 // Optional, default: no proxy server
 proxy: {
 host: 'localhost',
 port: 8888
 },
 
 // Your e-mail address, required for submitting ISRCs
 appMail: string
 }
 */

class MusicBrainz: Provider, Discovery {
    
    var id: ProviderId = ProviderId.musicbrainz
    
    /// MARK: Discovery
    func searchNewReleases() -> Promise<DiscoveryAlbumsResult> {
        return Promise { resolve, reject in
            let result = DiscoveryAlbumsResult()
            self.log("🥁... searchNewReleases ")
            
            resolve(result)
        }
    }
    
    func searchAlbum(id: EntityId) -> Promise<Album?> {
        return Promise { resolve, reject in
            let result: Album? = nil
            self.log("🥁... searchAlbum \(id) ")
            
            resolve(result)
        }
    }
    
    ///
    /// "http://musicbrainz.org/ws/2/release-group/?query=\"From Afar\"%20AND%20type:album%20AND%20artistname=Ensiferum"
    ///
    func searchAlbum(albumByband: AlbumByBand, bandId: EntityId?) -> Promise<Album?> {
        return Promise { resolve, reject in
            let result: Album? = nil
            self.log("🥁... searchAlbum \(albumByband.album) by \(albumByband.band) ")
            resolve(result)
        }
        
    }
    
    func searchBand(id: EntityId) -> Promise<Band?> {
        return Promise { resolve, reject in
            let result: Band? = nil
            self.log("🥁... searchBand")
            resolve(result)
        }
    }
    
    
    func searchPlaylists() -> Promise<[Playlist]> {
        return Promise { resolve, reject in
            let result: [Playlist] = []
            self.log("🥁... searchPlaylists")
            
            resolve(result)
        }
    }
    
}
