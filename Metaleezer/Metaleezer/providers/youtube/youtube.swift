//
//  youtube.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/4/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import Then
import SwiftDate
import SwiftSoup

/*
 https://music.youtube.com/search?q=insomnium+winter%27s+gate
 
 top result
 https://music.youtube.com/playlist?list=OLAK5uy_nzHf9CVopTdZ8ET_vvxbSQEgl869MVrwA
 -->https://youtube.com./playlist?list=OLAK5uy_nzHf9CVopTdZ8ET_vvxbSQEgl869MVrwA
 
 browseid = MPREb_omX8i9q9cNg
 https://music.youtube.com/browse/MPREb_omX8i9q9cNg
 key = AIzaSyC9XL3ZjWddXya6X74dJoCTL-WEYFDNX30
 https://music.youtube.com/youtubei/v1/browse?alt=json&key=AIzaSyC9XL3ZjWddXya6X74dJoCTL-WEYFDNX30
 
 
 
 */


typealias Map = [String:Any]

class Youtube: Provider, Discovery {
    var id: ProviderId = ProviderId.youtube
    , bandId: EntityId?
    /// MARK: Discovery
    func searchNewReleases() -> Promise<DiscoveryAlbumsResult> {
        return Promise { resolve, reject in
            asyncOn {
                let result = DiscoveryAlbumsResult()
                self.log("🥁... searchNewReleases ")
                resolve(result)
            }.onError { e in
                reject(e)
            }
        }
    }
    
    
    private func searchAlbumKey(browseId: String) -> Promise<String> {
        return Promise { resolve, reject in
            ///let result: String = TestAlbum().youtube.key
            
            self.log("🥁... searchAlbumKey")
            
            let c = URLComponents(string: "https://music.youtube.com/browse/\(browseId)")
            
            guard let url = c?.url else {
                reject(RequestError.badRequest)
                return
            }
            
            var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringCacheData)
            request.httpMethod = "GET"
            
            /// headers
            request.setValue("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36", forHTTPHeaderField: "user-agent")
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                    reject(error ?? RequestError.badRequest)
                    return
                }
                
                guard let s = String(data: data, encoding: String.Encoding.utf8) else {
                    reject(error ?? RequestError.badRequest)
                    return
                }
                
                let key = "INNERTUBE_API_KEY"
                if let r = s.range(of: key) {
                    let m = s[Range(uncheckedBounds: (lower: r.lowerBound, s.endIndex))]
                    if let f = m.firstIndex(of: ",") {
                        let q = s[Range(uncheckedBounds: (lower: r.lowerBound, f))]
                        let c = q.components(separatedBy: ":")
                        if c.count == 2 {
                            let key = c[1].trimmingCharacters(in: CharacterSet(charactersIn: "\""))
                            print("🐡 albumKey: \(key)")
                            resolve(key)
                        }
                    }
                }
                
            }.resume()
            
        }
    }
    
    private func searchAlbumByNameKey(query: String) -> Promise<(String,String)> {
        return Promise { resolve, reject in
            
            self.log("🥁... searchAlbumByNameKey")
            
            var c = URLComponents(string: "https://music.youtube.com/search?")
            c?.queryItems?.append(URLQueryItem(name: "query", value: query ))
            
            guard let url = c?.url else {
                reject(RequestError.badRequest)
                return
            }
            
            var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringCacheData)
            request.httpMethod = "GET"
            
            /// headers
            request.setValue("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36", forHTTPHeaderField: "user-agent")
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                    reject(error ?? RequestError.badRequest)
                    return
                }
                
                guard let s = String(data: data, encoding: String.Encoding.utf8) else {
                    reject(error ?? RequestError.badRequest)
                    return
                }
                
                let key = "INNERTUBE_API_KEY"
                if let r = s.range(of: key) {
                    let m = s[Range(uncheckedBounds: (lower: r.lowerBound, s.endIndex))]
                    if let f = m.firstIndex(of: ",") {
                        let q = s[Range(uncheckedBounds: (lower: r.lowerBound, f))]
                        let c = q.components(separatedBy: ":")
                        if c.count == 2 {
                            let key = c[1].trimmingCharacters(in: CharacterSet(charactersIn: "\""))
                            print("🐡 albumKey: \(key)")
                            resolve((key,url.absoluteString))
                        }
                    }
                }
                
            }.resume()
            
        }
    }
    
    
    private func buildSearchAlbumBody(browseId: String) -> Promise<Data> {
        return Promise { resolve, reject in
            
            guard let bodyPath = Bundle.main.path(forResource: "albumById.body", ofType: "json") else {
                reject(RequestError.badRequest)
                return
            }
            
            do {
                let bodyData = try Data(contentsOf: URL(fileURLWithPath: bodyPath), options: .mappedIfSafe)
                let bodyJson = try JSONSerialization.jsonObject(with: bodyData, options: .mutableLeaves)
                var bodyJsonObject = bodyJson as? Dictionary<String, Any>
                bodyJsonObject?["browseId"] = browseId
                
                guard let bodyDictionary = bodyJsonObject else {
                    reject(RequestError.badRequest)
                    return
                }
                
                let data = try JSONSerialization.data(withJSONObject: bodyDictionary, options: .prettyPrinted)
                resolve(data)
                
            }catch {
                reject(error)
                return
            }
            
        }
    }
    
    private func buildSearchAlbumByNameBody(query: String) -> Promise<Data> {
        return Promise { resolve, reject in
            
            guard let bodyPath = Bundle.main.path(forResource: "albumByName.body", ofType: "json") else {
                reject(RequestError.badRequest)
                return
            }
            
            do {
                let bodyData = try Data(contentsOf: URL(fileURLWithPath: bodyPath), options: .mappedIfSafe)
                let bodyJson = try JSONSerialization.jsonObject(with: bodyData, options: .mutableLeaves)
                var bodyJsonObject = bodyJson as? Dictionary<String, Any>
                bodyJsonObject?["query"] = query
                
                guard let bodyDictionary = bodyJsonObject else {
                    reject(RequestError.badRequest)
                    return
                }
                
                let data = try JSONSerialization.data(withJSONObject: bodyDictionary, options: .prettyPrinted)
                resolve(data)
                
            }catch {
                reject(error)
                return
            }
            
        }
    }
    
    ///
    ///
    func searchAlbum(id: EntityId) -> Promise<Album?> {
        
       return Promise { resolve, reject in
            asyncOn {
                self.log("🥁... searchAlbum \(id) ")
                
                let key = try ..self.searchAlbumKey(browseId: id)
                
                var c = URLComponents(string: "https://music.youtube.com/youtubei/v1/browse?")
                c?.queryItems?.append(URLQueryItem(name: "alt", value: "json" ))
                c?.queryItems?.append(URLQueryItem(name: "key", value: key ))
                
                guard let url = c?.url else {
                    reject(RequestError.badRequest)
                    return
                }
                
                var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringCacheData)
                request.httpMethod = "POST"
                
                /// headers
                request.setValue("https://music.youtube.com/browse/\(id)", forHTTPHeaderField: "referer")
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("https://music.youtube.com", forHTTPHeaderField: "origin")
                
                /// body
                let body = try ..self.buildSearchAlbumBody(browseId: id)
                request.setValue(String(body.count), forHTTPHeaderField: "Content-Length")
                request.httpBody = body
                
                URLSession.shared.dataTask(with: request) { (data, response, error) in
                    guard let data = data else {
                        reject(error ?? RequestError.badRequest)
                        return
                    }
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any]
                        
                        guard let a = json?["frameworkUpdates"] as? [String:Any],
                            let b = a["entityBatchUpdate"] as? [String:Any],
                            let c = b["mutations"] as? [[String:Any]] else {
                                reject(error ?? RequestError.badRequest)
                                return
                        }
                        
                        let tracks = c.compactMap { (item: [String : Any]) -> [String:Any]? in
                            guard let payload = item["payload"] as? [String:Any],
                                let musicTrack = payload["musicTrack"] as? [String:Any] else {
                                    return nil
                            }
                            return musicTrack
                        }
                        
                        let songs = tracks.compactMap { (item: [String : Any]) -> Song? in
                            guard let id = item["videoId"] as? String, let name = item["title"] as? String else {
                                return nil
                            }
                            return Song(
                                id: [.youtube : id],
                                album: id,
                                name: name,
                                streamLink: [.youtube : "http://youtube.com./watch?v=\(id)"]
                            )
                        }
                        
                        let album = Album(
                            id: [.youtube : id],
                            band: "",
                            name: "",
                            cover: "",
                            date: nil,
                            tags: [],
                            duration: nil,
                            songs: songs)
                        
                        resolve(album)
                    }catch {
                        reject(error)
                    }
                    
                }.resume()
                
            }.onError { (e) in
                reject(e)
            }
        }
    }
    
    
    
    
    ///
    ///
    func searchAlbum(albumByband: AlbumByBand, bandId: EntityId?) -> Promise<Album?> {
        
        return Promise { resolve, reject in
            asyncOn {
                self.log("🥁... searchAlbum")
                                
                var album = albumByband.album
                var band = albumByband.band
                
                if useTestAlbum {
                    album = TestAlbum().album.album
                    band = TestAlbum().album.band
                }
                
                let query = "\(album) \(band)"
                
                let (key,referer) = try ..self.searchAlbumByNameKey(query: query)
                
                var c = URLComponents(string: "https://music.youtube.com/youtubei/v1/search?")
                c?.queryItems?.append(URLQueryItem(name: "alt", value: "json" ))
                c?.queryItems?.append(URLQueryItem(name: "key", value: key ))
                
                guard let url = c?.url else {
                    reject(RequestError.badRequest)
                    return
                }
                
                var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringCacheData)
                request.httpMethod = "POST"
                
                /// headers
                request.setValue(referer, forHTTPHeaderField: "referer")
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("https://music.youtube.com", forHTTPHeaderField: "origin")
                
                /// body
                let body = try ..self.buildSearchAlbumByNameBody(query: query)
                request.setValue(String(body.count), forHTTPHeaderField: "Content-Length")
                request.httpBody = body
                
                URLSession.shared.dataTask(with: request) { (data, response, error) in
                    guard let data = data else {
                        reject(error ?? RequestError.badRequest)
                        return
                    }
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any]
                        
                        guard let a = json?["contents"] as? Map,
                            let b = a["sectionListRenderer"] as? Map,
                            let c = b["contents"] as? [Map] else {
                                reject(error ?? RequestError.badRequest)
                                return
                        }
                        
                        let musicShelfRenderer = c.compactMap { (item: Map) -> Map? in
                            guard let musicShelfRenderer = item["musicShelfRenderer"] as? Map,
                                let title = musicShelfRenderer["title"] as? Map,
                                let runs = title["runs"] as? [Map],
                                let text = runs.first?["text"] as? String, text == "Albums" else {
                                    return nil
                            }
                            
                            return musicShelfRenderer
                        }.first
                        
                        guard let albums = musicShelfRenderer?["contents"] as? [Map] else {
                            reject(error ?? RequestError.badRequest)
                            return
                        }
                        
                        
                        let youtubeAlbumId = albums.compactMap { (item: Map) -> String? in
                            guard let musicResponsiveListItemRenderer = item["musicResponsiveListItemRenderer"] as? Map,
                                let navigationEndpoint = musicResponsiveListItemRenderer["navigationEndpoint"] as? Map,
                                let browseEndpoint = navigationEndpoint["browseEndpoint"] as? Map else {
                                return nil
                            }
                            
                            if let columns = musicResponsiveListItemRenderer["flexColumns"] as? [Map] {
                                let validation = columns.filter { (column: Map) -> Bool in
                                    guard let musicResponsiveListItemFlexColumnRenderer = column["musicResponsiveListItemFlexColumnRenderer"] as? Map,
                                        let text = musicResponsiveListItemFlexColumnRenderer["text"] as? Map,
                                        let runs = text["runs"] as? [Map],
                                        let value = runs.first?["text"] as? String, (value == "Album" || value == album) else {
                                        return false
                                    }
                                    return true
                                }
                                
                                if validation.count != 2 {
                                    return nil
                                }
                            }
                            
                            
                            return browseEndpoint["browseId"] as? String
                        }.first
                        
                        
                        if let albumId = youtubeAlbumId {
                            resolve(Album(id: [.youtube:albumId], band: "", name: album))
                            return
                        }
                        
                        reject(RequestError.badRequest)
                        
                    }catch {
                        reject(error)
                    }
                    
                }.resume()
                
            }.onError { e in
                reject(e)
            }
        }
        
    }
    
    func searchBand(id: EntityId) -> Promise<Band?> {
        return Promise { resolve, reject in
            let result: Band? = nil
            self.log("🥁... searchBand")
            
            resolve(result)
        }
    }
    
    func searchPlaylists() -> Promise<[Playlist]> {
        return Promise { resolve, reject in
            let result: [Playlist] = []
            self.log("🥁... searchPlaylists")
            
            resolve(result)
        }
    }
    
    /// MARK: Lyrics
    func searchLyrics(id: EntityId?, songName: String?, albumByband: AlbumByBand) -> Promise<String?> {
        return Promise { resolve, reject in
            let result: String? = nil
            ///
            self.log("🥁... searchLyrics for song \(songName ?? "") in ")
            
            resolve(result)
        }
    }
}


