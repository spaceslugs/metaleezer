//
//  Actions.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/6/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import Foundation
import ReSwift
import ReSwift_Thunk



struct Actions {
    
    enum User: Action {
        case  changeTheme(String)
    }
    
    enum Player: Action {
        case song(Song)
        case album(AlbumExtended,[Song])
        case nextAlbumSong(Song)
        case lyrics(String)
        case pauseTime(Double)
        case playing(Bool)
        case failure(StateError)
        case finishPlay
    }
    
    enum Shared: Action {
        case fetchCoverUrlStart(EntityId)
        case fetchCoverUrlSuccess(EntityId,String)
        case fetchCoverUrlFailure(EntityId,StateError)
        
        case likeAlbum(AlbumExtended?)
        case notifyMe(AlbumExtended?)
        case likeSong(Song?)
        
    }
    
    
    /// pages
    
    enum Splash: Action {
        case  appLoadingStart
        case  appLoadingSuccess
        case  appLoadingFailure(StateError)
    }
    
    enum Home: Action {
        case  searchNewReleasesStart
        case  searchNewReleasesSuccess([Review],[AlbumCollection]?)
        case  searchNewReleasesFailure(StateError)
        case  selectTag(String)
    }
    
    enum AlbumList: Action {
        case  openStart
        case  openSuccess(AlbumCollection)
        case  openFailure(StateError)
        case  selectTag(String)    
    }
    
    enum Album: Action {
        case  openStart
        case  openSuccess(AlbumExtended)
        case  openFailure(StateError)
    }
    
}




