//
//  albumListPageReducer.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/15/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import ReSwift
import SwiftDate


func albumListPageReducer(action: Action, state: AlbumListPageState?) -> AlbumListPageState {
    var state = state ?? AlbumListPageState()
    
    func filterAlbums() -> [AlbumExtended] {
        return  state.collection?.albums.filterAlbums(by: state.tags) ?? []
    }
    
    switch action {
    case Actions.AlbumList.openStart:
        state.loading = true
        state.error = nil
    case Actions.AlbumList.openSuccess(let collection):
        state.loading = false
        state.collection = collection
        state.filteredAlbums = filterAlbums()
    case Actions.AlbumList.openFailure(let error):
        state.loading = false
        state.error = error
    case Actions.AlbumList.selectTag(let tag):
        state.tags[tag] = (state.tags[tag] ?? false) ? false : true
        state.filteredAlbums = filterAlbums()
    default:
        break
    }
    
    return state
}

