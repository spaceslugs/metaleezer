//
//  albumPageReducer.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/15/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import ReSwift
import SwiftDate

func albumPageReducer(action: Action, state: AlbumPageState?) -> AlbumPageState {
    var state = state ?? AlbumPageState()
    
    switch action {
     case Actions.Album.openStart:
         state.loading = true
         state.error = nil
         state.album = nil
     case Actions.Album.openSuccess(let album):
         state.loading = false
         state.album = album
     case Actions.Album.openFailure(let error):
         state.loading = false
         state.error = error
     default:
         break
     }
    
    return state
}
