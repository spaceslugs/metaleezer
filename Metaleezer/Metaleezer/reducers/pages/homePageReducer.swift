//
//  homePageReducer.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/13/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import Foundation
import ReSwift
import SwiftDate


/// searchTodayReleases
func searchTodayReleases() -> AlbumCollection {
    
    let startDate = Date()
    print("⏰", #function, #line)
    
    let albums = System.music.albums.filter {
        $0.value.date?.toDate()?.isToday ?? false
    }
    .map { AlbumExtended($0.value) }
    .sortAlbumsByRecent()
    
    print("⏰", #function, #line, Date().timeIntervalSince(startDate))
    
    return AlbumCollection(title: "Today releases", albums: albums )
}


/// searchLastWeekReleases
func searchLastWeekReleases() -> AlbumCollection {

    let startDate = Date()
    print("⏰", #function, #line)

    
    let albums = System.music.albums.filter {
        let end = DateInRegion().dateAt(.yesterdayAtStart)
        let start = end.dateByAdding(-6, Calendar.Component.day)
        return $0.value.date?.toDate()?.isInRange(date: start, and: end, orEqual: true) ?? false
    }
    .map { AlbumExtended($0.value) }
    .sortAlbumsByRecent()
    
    print("⏰", #function, #line, Date().timeIntervalSince(startDate))
    
    return AlbumCollection(title: "Last week releases", albums: albums)
}

/// searchUpcomingReleases
func searchUpcomingReleases() -> AlbumCollection {
    
    let startDate = Date()
    print("⏰", #function, #line)
    
    let albums = System.music.albums.filter {
        $0.value.date?.toDate()?.isInFuture ?? false
    }
    .map { AlbumExtended($0.value) }
    .sortAlbumsByClosestDate()
      
    print("⏰", #function, #line, Date().timeIntervalSince(startDate))
    
    return AlbumCollection(title: "Upcoming releases", albums: albums )
}

/// searchRecentlyReviewed
func searchRecentlyReviewed() -> AlbumCollection {
    /// todo
    return AlbumCollection(title: "Recently reviewed releases", albums: [])
}

/// searchCollections
func searchCollections() -> [AlbumCollection] {
    return [
        searchTodayReleases(),
        searchLastWeekReleases(),
        searchUpcomingReleases(),
        ///searchRecentlyReviewed()
    ]
}

func homePageReducer(action: Action, state: HomePageState?) -> HomePageState {
    var state = state ?? HomePageState()
    
    func filterCollections() -> [AlbumCollection] {
        state.collections.map {
            let albums = $0.albums.filterAlbums(by: state.tags)
                .limitAlbums(by: 20)
            return AlbumCollection(title: $0.title, albums: albums )
        }
    }
    
    switch action {
    case Actions.Home.searchNewReleasesStart:
        state.loading = true
        state.error = nil
    case Actions.Home.searchNewReleasesSuccess(let reviews, let collections):
        /// 🎸 create all filtered collections using music db.
        state.reviews = reviews
        if let collections = collections {
            state.collections = collections
        }
        state.filteredCollections = filterCollections()
        state.loading = false
        state.error = nil
    case Actions.Home.searchNewReleasesFailure(let error):
        state.loading = false
        state.error = error
    case Actions.Home.selectTag(let tag):
        state.tags[tag] = (state.tags[tag] ?? false) ? false : true
        state.filteredCollections = filterCollections()
    default:
        break
    }
    
    return state
}

