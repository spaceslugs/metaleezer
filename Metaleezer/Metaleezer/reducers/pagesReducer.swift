
import Foundation
import ReSwift
import SwiftDate

/// other pages

func splashPageReducer(action: Action, state: SplashPageState?) -> SplashPageState {
    let state = state ?? SplashPageState()
    return state
}

func favsPageReducer(action: Action, state: FavsPageState?) -> FavsPageState {
    let state = state ?? FavsPageState()
    return state
}

func reviewsPageReducer(action: Action, state: ReviewsPageState?) -> ReviewsPageState {
    let state = state ?? ReviewsPageState()
    return state
}

func gigsPageReducer(action: Action, state: GigsPageState?) -> GigsPageState {
    let state = state ?? GigsPageState()
    return state
}

func profilePageReducer(action: Action, state: ProfilePageState?) -> ProfilePageState {
    let state = state ?? ProfilePageState()
    return state
}

func searchPageReducer(action: Action, state: SearchPageState?) -> SearchPageState {
    let state = state ?? SearchPageState()
    return state
}

func bandPageReducer(action: Action, state: BandPageState?) -> BandPageState {
    let state = state ?? BandPageState()
    return state
}



func gigPageReducer(action: Action, state: GigPageState?) -> GigPageState {
    let state = state ?? GigPageState()
    return state
}





func pagesReducer(action: Action, state: PageState?) -> PageState {
    return PageState(
        splash: splashPageReducer(action: action, state: state?.splash),
        home: homePageReducer(action: action, state: state?.home),
        favs: favsPageReducer(action: action, state: state?.favs),
        reviews: reviewsPageReducer(action: action, state: state?.reviews),
        gigs: gigsPageReducer(action: action, state: state?.gigs),
        profile: profilePageReducer(action: action, state: state?.profile),
        search: searchPageReducer(action: action, state: state?.search),
        band: bandPageReducer(action: action, state: state?.band),
        album: albumPageReducer(action: action, state: state?.album),
        albumList: albumListPageReducer(action: action, state: state?.albumList),
        gig: gigPageReducer(action: action, state: state?.gig))
}

