
import Foundation
import ReSwift

func playerReducer(action: Action, state: PlayerState?) -> PlayerState {
    var state = state ?? PlayerState()
    
    switch action {
    case Actions.Player.album(let album, let queue):
        state.album = album
        state.queue = queue
        state.song = queue.first
        state.error = nil
    case Actions.Player.nextAlbumSong(let song):
        state.song = song
        state.error = nil
    case Actions.Player.song(let song):
        state.song = song
        state.album = nil
        state.queue = []
        state.error = nil
    case Actions.Player.finishPlay:
        state.song = nil
        state.album = nil
        state.error = nil
    case Actions.Player.playing(let isPlaying):
        state.playing = isPlaying
    case Actions.Player.lyrics(let lyrics):
        state.lyrics = lyrics
    case Actions.Player.pauseTime(let pauseTime):
        state.pauseTime = pauseTime
    case Actions.Player.failure(let error):
        state.album = nil
        state.song = nil
        state.error = error
    default:
        break
    }
    
    return state
}
