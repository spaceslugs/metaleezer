//
//  sharedReducer.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/14/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//


import Foundation
import ReSwift

func sharedReducer(action: Action, state: SharedState?) -> SharedState {
    var state = state ?? SharedState()

    switch action {
    case Actions.Shared.fetchCoverUrlStart(let albumId):
        state.coverUrl[albumId] = Cover(url: nil, loading: true, error: nil)
    case Actions.Shared.fetchCoverUrlSuccess(let albumId,let url):
        state.coverUrl[albumId]?.url = url
        state.coverUrl[albumId]?.loading = false
        state.coverUrl[albumId]?.error = nil
    case Actions.Shared.fetchCoverUrlFailure(let albumId,let error):
        state.coverUrl[albumId]?.error = error
        state.coverUrl[albumId]?.loading = false
        state.coverUrl[albumId]?.url = nil
    default:
        break
    }

    return state
}
