//
//  AppState.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/6/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import ReSwift


/// app state
struct AppState: PersistState {
    var user: UserState
    var pages: PageState
    var shared: SharedState
    var player: PlayerState
}

/// helpers for AppState. uses main global store and  AppState state
class AppStateProp<T: Equatable>: StateProp<T,AppState> {
    init(_ selector: @escaping ((_ state: AppState) -> T)) {
        super.init(store: store, selector:selector)
    }

    override func didUpdate(_ f: @escaping ((_ value: T) -> ()) ) { super.didUpdate(f) }
    override func unsubscribe() { super.unsubscribe() }
    
}

typealias Prop<T: Equatable> = AppStateProp<T>

