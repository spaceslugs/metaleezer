//
//  appStore.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/9/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import ReSwift
import ReSwift_Thunk

/// persist configurations
var persistConfig: PersistConfig = {
    var config = PersistConfig(persistDirectory: "data", version: "1")
    config.debug = true
    return config
}()

/// app reducers
let persistUserReducer = persistReducer(config: persistConfig, baseReducer:  userReducer)
let persistPagesReducer = persistReducer(config: persistConfig, baseReducer:  pagesReducer)
let persistSharedReducer = persistReducer(config: persistConfig, baseReducer:  sharedReducer)
let persistPlayerReducer = persistReducer(config: persistConfig, baseReducer:  playerReducer)

func appReducer(action: Action, state: AppState?) -> AppState {
    return AppState(
        user: persistUserReducer(action,state?.user),
        pages: persistPagesReducer(action,state?.pages),
        shared: persistSharedReducer(action,state?.shared),
        player: persistPlayerReducer(action,state?.player)
    )
}


/// create middlewares
let thunkMiddleware: Middleware<AppState> = createThunkMiddleware()

/// create app store
var store = Store<AppState>(reducer: appReducer, state: nil, middleware: [thunkMiddleware])

///helper for main store. dispatches action on main thread and main store
struct AppStore {
    static func dispatch(_ action: Action) {
        DispatchQueue.main.async {
            store.dispatch(action)
        }
    }
}
