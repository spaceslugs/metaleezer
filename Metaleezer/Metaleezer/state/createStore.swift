//
//  CreateStore.swift
//  ACE Driver
//
//  Created by Darko Pavlovic on 4/27/20.
//  Copyright © 2020 ComputerRock. All rights reserved.
//

import Foundation
import ReSwift
import ReSwiftThunk

/// persist configurations
var persistConfig: PersistConfig = {
    var config = PersistConfig(persistDirectory: "data", version: "1")
    config.debug = true
    return config
}()

var sensitivePersistConfig: PersistConfig = {
    var config = PersistConfig(persistDirectory: "keychain", version: "1")
    config.debug = true
    return config
}()


/// app reducers
let persistAuthReducer = persistReducer(config: sensitivePersistConfig, baseReducer:  authReducer)
let persistUserReducer = persistReducer(config: persistConfig, baseReducer:  userReducer)
let persistDamageEventReducer = persistReducer(config: persistConfig, baseReducer:  damageEventReducer)
let persistConfigReducer = persistReducer(config: persistConfig, baseReducer:  configReducer)
     
func appReducer(action: Action, state: AppState?) -> AppState {
    return AppState(
        auth: persistAuthReducer(action,state?.auth),
        user: persistUserReducer(action,state?.user),
        damageEvent: persistDamageEventReducer(action,state?.damageEvent),
        config: persistConfigReducer(action,state?.config)
    )
}


/// create middlewares
let thunkMiddleware: Middleware<AppState> = createThunkMiddleware()

/// create app store
var store = Store<AppState>(reducer: appReducer, state: nil, middleware: [thunkMiddleware])

///helper for main store. dispatches action on main thread and main store
struct AppStore {
    static func dispatch(_ action: Action) {
        DispatchQueue.main.async {
            store.dispatch(action)
        }
    }
}
