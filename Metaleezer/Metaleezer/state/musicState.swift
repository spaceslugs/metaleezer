//
//  musicState.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/9/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation
import SwiftDate
import Fuse

var persistMusicConfig: PersistConfig = {
    var config = PersistConfig(persistDirectory: "music", version: "1")
    config.debug = true
    return config
}()

struct MusicState: PersistState {
    var albums: [EntityId : Album] = [:]
    var bands: [EntityId : Band] = [:]
    
    var lastUpdateDate: String? = nil
    
    mutating func clearDiscovery() {
        albums = [:]
        bands = [:]
    }
    
    mutating func addDiscovery(data: DiscoveryAlbumsResult) {
        print("⏰", #function, #line)
        
        data.bands.forEach {
            /// discovery always add new one, no update
            bands[$0.key] = $0.value
        }
        
        data.albums.forEach {
            /// discovery always add new one, no update
            albums[$0.key] = $0.value
            
            /// link album to band
            albums[$0.key]?.band = $0.value.band
            /// add(link) album to band
            if bands[$0.value.band]?.albums.firstIndex(of: $0.key) == nil {
                bands[$0.value.band]?.albums.append($0.key)
            }
        }
        
        lastUpdateDate = DateInRegion().toISO()
        
        print("⏰", #function, #line)
    }
    
    
    mutating func updateBand(bandId: EntityId?, update: Band) {
        guard let bandId = bandId else {
            return
        }

        bands[bandId]?.lastUpdateDate = DateInRegion().toISO()
        
        // update provider ids discovered
        update.id.forEach {
            let (key,value) = $0
            bands[bandId]?.id[key] = value
        }
        
        /// ...
    }
    
    mutating func updateAlbum(albumId: EntityId?, update: Album) {
        guard let albumId = albumId else {
            return
        }
                
        let provider = update.id.first?.key ?? ProviderId.metalarchives
        let albumProvider =  ProviderId.metalarchives
        
        // update provider ids discovered
        update.id.forEach {
            let (key,value) = $0
            albums[albumId]?.id[key] = value
        }
        
        if let value = update.duration {
            albums[albumId]?.duration = value
        }

        update.tags.forEach { tag in
            guard let _ = albums[albumId]?.tags.first(where: { (tagName) -> Bool in
                tagName == tag
            }) else {
                albums[albumId]?.tags.append(tag)
                return
            }
        }
        
        guard provider != albumProvider else {
            albums[albumId]?.songs = update.songs.map {
                Song(id: $0.id,
                    album: albumId, /// albumId is primaryId(metalarchives)
                    name: $0.name,
                    duration: $0.duration,
                    streamLink: $0.streamLink,
                    lyricsLink: $0.lyricsLink,
                    previewLink: $0.previewLink
                )
            }
            
            return
        }
         
        
        update.songs.forEach {
            if let info = self.song(by: $0.name, in: albumId) {
                let (src,index) = info
                var dst = src
                
                $0.id.forEach { (id) in
                    let (key,value) = id
                    dst.id[key] = value
                }
                
                if let value  = $0.duration {
                    dst.duration = value
                }
                if let value  = $0.streamLink[provider] {
                    dst.streamLink[provider] = value
                }
                if let value  = $0.lyricsLink[provider] {
                    dst.lyricsLink[provider] = value
                }
                if let value  = $0.previewLink {
                    dst.previewLink = value
                }

                albums[albumId]?.songs[index] = dst
            }
        }
                   
    }
    
    private func song(by name: String, in albumId: EntityId) -> (Song,Int)? {
        
        guard let album = albums[albumId] else {
            return nil
        }
        
        let fuse = Fuse()
                
        if let index = album.songs.firstIndex(where: { (song) -> Bool in
            var result = fuse.search(name, in: song.name)
            if result == nil {
                result = fuse.search(song.name, in: name)
            }
            ///print("🍌: search <\(name)> in <\(song.name)> -> \(result?.score ?? 1.0)")
            return (result?.score ?? 1.0) < 0.1 ? true : false
            //return song.name.caseInsensitiveCompare(name) == .orderedSame            
        }) {
            return (album.songs[index],index)
        }
        
        return nil
    }
    
    
    func areCompletedStreamLinks(for albumId: EntityId) -> Bool {
        guard let album = albums[albumId], album.songs.count > 0 else {
            return false
        }
        
        let result = album.songs.reduce(0) { r, s in
            r + ((s.streamLink.count > 0) ? 1 : 0)
        }

        return result == album.songs.count
        
    }
    
    
    func bandId(for albumId: EntityId, and provider: ProviderId) ->EntityId? {
        guard let album = albums[albumId] else {
            return nil
        }
        
        return bands[album.band]?.id[provider]
    }
    
}

