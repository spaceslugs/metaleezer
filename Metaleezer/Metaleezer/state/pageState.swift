//
//  PageState.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/9/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation

/// default tags state
let defaultTags: [String : Bool] = [
    "death" : false,
    "power" : false,
    "progressive" : false,
    "melodic death" : false,
    "heavy" : false,
    "grindcore" : false,
    "thrash" : false,
    "groove" : false,
    "black" : false,
    "doom" : false,
    "industrial" : false,
    "folk" : false,
    "gothic" : false,
    "metalcore" : false,
    "symphonic" : false,
    "speed" : false,
]


struct SplashPageState: PersistState {
    var loading: Bool = false
    var error: StateError? = nil
}


struct HomePageState: PersistState {
    var loading: Bool = false
    var error: StateError? = nil
    var reviews: [Review] = []
    var collections: [AlbumCollection] = []
    var filteredCollections: [AlbumCollection] = []
    var tags: [String : Bool] = defaultTags
}

struct FavsPageState: PersistState {
    var loading: Bool = false
    var error: StateError? = nil
}

struct ReviewsPageState: PersistState {
    var loading: Bool = false
    var error: StateError? = nil
}

struct GigsPageState: PersistState {
    var loading: Bool = false
    var error: StateError? = nil
}

struct ProfilePageState: PersistState {
    var loading: Bool = false
    var error: StateError? = nil
}

struct SearchPageState: PersistState {
    var loading: Bool = false
    var error: StateError? = nil
}

struct BandPageState: PersistState {
    var loading: Bool = false
    var error: StateError? = nil
}

struct AlbumPageState: PersistState {
    var loading: Bool = false
    var error: StateError? = nil
    var album: AlbumExtended? = nil
}

struct AlbumListPageState: PersistState {
    var loading: Bool = false
    var error: StateError? = nil
    var collection: AlbumCollection?
    var filteredAlbums: [AlbumExtended] = []
    var tags: [String : Bool] = [:]
}

struct GigPageState: PersistState {
    var loading: Bool = false
    var error: StateError? = nil
}


struct PageState: PersistState {
    var splash: SplashPageState
    var home: HomePageState
    var favs: FavsPageState
    var reviews: ReviewsPageState
    var gigs: GigsPageState
    var profile: ProfilePageState
    var search: SearchPageState
    var band: BandPageState
    var album: AlbumPageState
    var albumList: AlbumListPageState
    var gig: GigPageState
}

