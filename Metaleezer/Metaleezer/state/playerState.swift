//
//  playerState.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/9/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation

struct PlayerState: PersistState {
    var album: AlbumExtended?
    var queue: [Song] = []
    var song: Song?
    var lyrics: String?
    var pauseTime: Double = 0
    var playing: Bool = false
    var error: StateError?
}

