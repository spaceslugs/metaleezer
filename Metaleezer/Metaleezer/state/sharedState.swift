//
//  SharedState.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/14/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation


struct Cover: PersistState {
    var url: String?
    var loading = false
    var error: StateError?
}


struct SharedState: PersistState {
    var coverUrl: [EntityId : Cover] = [:]
}
