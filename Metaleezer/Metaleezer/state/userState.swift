//
//  userState.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/9/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation


struct UserState: PersistState {
    var theme: String = "Dark"
}
