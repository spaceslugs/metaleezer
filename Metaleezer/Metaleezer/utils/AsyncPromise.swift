//
//  asyncFlow.swift
//  eAccess
//
//  Created by Darko Pavlovic on 7/5/22.
//

import Foundation
import Dispatch
import Then

@discardableResult
public func asyncOn<T>(_ queue: DispatchQueue = DispatchQueue.global() , block:@escaping () throws -> T) -> Async<T> {
    let p = Promise<T> { resolve, reject in
        queue.async {
            do {
                let t = try block()
                resolve(t)
            } catch {
                reject(error)
            }
        }
    }
    p.start()
    return p
}
