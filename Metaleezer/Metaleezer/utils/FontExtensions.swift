//
//  Font+List.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/10/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import UIKit

extension UIFont {
    static func printAll() {
        UIFont.familyNames.forEach {
            UIFont.fontNames(forFamilyName: $0).forEach { (name) in
                print("🐶 [\(name)]")
            }
        }
    }
}

        
