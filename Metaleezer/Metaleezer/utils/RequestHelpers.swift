//
//  RequestHelpers.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/14/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import Foundation


func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - URLSession response handlers

extension URLSession {
    func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            
            do {
              let decoded = try JSONDecoder().decode(T.self, from: data)
              completionHandler(decoded, response, nil)
            } catch {
                print("🍌: \(error)")
                completionHandler(nil, response, error)
            }
                        
        }
    }
}

