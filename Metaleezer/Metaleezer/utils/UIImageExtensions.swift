//
//  UIImage+AwesomeFont.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/10/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import UIKit


extension UIImage {

    static func icon(code: String, font: UIFont?, textColor: UIColor?, backgroundColor: UIColor = UIColor.clear, borderWidth: CGFloat = 0, borderColor: UIColor = UIColor.clear) -> UIImage {
        
        let imageFont = font ?? UIFont.boldSystemFont(ofSize: 16)
        let color = textColor ?? .white

        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = NSTextAlignment.center
                
        let fontAspectRatio: CGFloat = 1.28571429
        let size = CGSize(width: imageFont.pointSize * fontAspectRatio, height: imageFont.pointSize)
        let fontSize = imageFont.pointSize
        
        // stroke width expects a whole number percentage of the font size
        let strokeWidth: CGFloat = fontSize == 0 ? 0 : (-100 * borderWidth / fontSize)

        let attributedString = NSAttributedString(string: code, attributes: [
            NSAttributedString.Key.font: imageFont,
            NSAttributedString.Key.foregroundColor: color,
            NSAttributedString.Key.backgroundColor: backgroundColor,
            NSAttributedString.Key.paragraphStyle: paragraph,
            NSAttributedString.Key.strokeWidth: strokeWidth,
            NSAttributedString.Key.strokeColor: borderColor
            ])

        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        attributedString.draw(in: CGRect(x: 0, y: (size.height - fontSize) / 2, width: size.width, height: fontSize))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }

}
