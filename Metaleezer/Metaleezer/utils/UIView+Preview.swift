//
//  UIView+Preview.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 10/13/21.
//  Copyright © 2021 Darko Pavlovic. All rights reserved.
//

import UIKit
import SwiftUI

extension UIView {
    private struct Preview: UIViewRepresentable {

        let view: UIView
        
        func makeUIView(context: Context) -> UIView {
            return view
        }
        
        func updateUIView(_ uiView: UIView, context: Context) {}
    }
    
    func showPreview() -> some View {
        Preview(view: self)
    }
}
