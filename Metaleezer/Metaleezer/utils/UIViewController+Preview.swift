//
//  UIViewController+Preview.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 10/13/21.
//  Copyright © 2021 Darko Pavlovic. All rights reserved.
//

import SwiftUI

extension UIViewController {
    private struct Preview: UIViewControllerRepresentable {

        let viewController: UIViewController
        
        func makeUIViewController(context: Context) -> UIViewController {
            return viewController
        }
        
        func updateUIViewController(_ uiViewController: UIViewController, context: Context) {}
    }
    
    func showPreview() -> some View {
        Preview(viewController: self)
    }
}
