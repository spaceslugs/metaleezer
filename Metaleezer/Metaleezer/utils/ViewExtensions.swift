//
//  ViewExtensions.swift
//  Metaleezer
//
//  Created by Darko Pavlovic on 6/16/20.
//  Copyright © 2020 Darko Pavlovic. All rights reserved.
//

import UIKit

extension UIView {
    func findViewController() -> UIViewController? {
        if let nextResponder = self.next as? UIViewController {
            return nextResponder
        } else if let nextResponder = self.next as? UIView {
            return nextResponder.findViewController()
        } else {
            return nil
        }
    }
}
