//
//  Reselect.swift
//
//
//  Created by Darko Pavlovic on 5/5/20.
//  Copyright © 2020 ComputerRock. All rights reserved.
//

import Foundation

// http://blog.scottlogic.com/2014/09/22/swift-memoization.html
// http://stackoverflow.com/questions/31129211/need-detailed-explanation-for-memoize-implementation-in-swift-wwdc-14-session

func memoize<T: Hashable, U>(_ fn: @escaping (T) -> U) -> (T) -> U {
    var memo = Dictionary<T, U>()
    
    func result(selector: T) -> U {
        if let q = memo[selector] { return q }
        let r = fn(selector)
        memo[selector] = r
        return r
    }
    
    return result
}

public func createSelector<TInput: Hashable, TOutput, T1>(_ selector1:  @escaping (TInput) -> T1, _ combine:  @escaping (T1) -> TOutput) -> (TInput) -> TOutput {
    return memoize({ (state) in
        return combine(selector1(state))
    })
}

public func createSelector<TInput: Hashable, TOutput, T1, T2>(_ selector1:  @escaping (TInput) -> T1, _ selector2:  @escaping (TInput) -> T2, _ combine:  @escaping (T1, T2) -> TOutput) -> (TInput) -> TOutput {
    return memoize({ (state) in
        return combine(selector1(state), selector2(state))
    })
}

public func createSelector<TInput: Hashable, TOutput, T1, T2, T3>(_ selector1:  @escaping (TInput) -> T1, _ selector2:  @escaping (TInput) -> T2, selector3:  @escaping (TInput) -> T3, _ combine:  @escaping (T1, T2, T3) -> TOutput) -> (TInput) -> TOutput {
    return memoize({ (state) in
        return combine(selector1(state), selector2(state), selector3(state))
    })
}

public func createSelector<TInput: Hashable, TOutput, T1, T2, T3, T4>(_ selector1:  @escaping (TInput) -> T1, _ selector2:  @escaping (TInput) -> T2, selector3:  @escaping (TInput) -> T3, selector4:  @escaping (TInput) -> T4, _ combine:  @escaping (T1, T2, T3, T4) -> TOutput) -> (TInput) -> TOutput {
    return memoize({ (state) in
        return combine(selector1(state), selector2(state), selector3(state), selector4(state))
    })
}

public func createSelector<TInput: Hashable, TOutput, T1, T2, T3, T4, T5>(_ selector1:  @escaping (TInput) -> T1, _ selector2:  @escaping (TInput) -> T2, selector3:  @escaping (TInput) -> T3, selector4:  @escaping (TInput) -> T4, selector5:  @escaping (TInput) -> T5, _ combine:  @escaping (T1, T2, T3, T4, T5) -> TOutput) -> (TInput) -> TOutput {
    return memoize({ (state) in
        return combine(selector1(state), selector2(state), selector3(state), selector4(state), selector5(state))
    })
}

