//
//  SagaEffects.swift
//  ACE Driver
//
//  Created by Darko Pavlovic on 5/4/20.
//  Copyright © 2020 ComputerRock. All rights reserved.
//

import Foundation
import Then
import ReSwift

/*
 Note that the callback block from the network request arrives on a background thread, therefore we’re using DispatchQueue.main.async to perform the state update on the main thread.

 ReSwift will call reducers and subscribers on whatever thread you have dispatched an action from.

 We recommend to always dispatch from the main thread, but ReSwift does not enforce this recommendation. ReSwift will enforce that all Dispatches, Store Subscribes and Store Unsubscribes are on the same thread or serial Grand Central Dispatch queue.

 Therefore the main dispatch queue works, however the global dispatch queue, being concurrent, will fail.
*/


enum SideEffect {
    case put
}

/// dispatch action on main thread  using given dispatch function
func yield(_ effect: SideEffect, _ dispatch: @escaping DispatchFunction, _ action: Action) {
    switch effect {
    case .put:
        DispatchQueue.main.async {
            dispatch(action)
        }
    }
}

/// dispatch  action on main thread and on main store
func yield(_ effect: SideEffect, _ action: Action) {
    switch effect {
    case .put:
        AppStore.dispatch(action)
    }
}
