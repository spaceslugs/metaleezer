//
//  StateProp.swift
//  ACE Driver
//
//  Created by Darko Pavlovic on 5/4/20.
//  Copyright © 2020 ComputerRock. All rights reserved.
//

import Foundation
import ReSwift


class StateProp<T: Equatable, S: StateType>: StoreSubscriber {
    private var _store: Store<S>
    private var _selector: ((_ state: S) -> T)
    private var _didUpdate: ((_ value: T) -> ())?
    private var _updateCount = 0

    init(store: Store<S>, selector: @escaping ((_ state: S) -> T)) {
        self._store = store
        self._selector = selector
    }
        
    func unsubscribe() {
        _store.unsubscribe(self)
    }
    
    func newState(state: T) {
        _updateCount += 1
        self._didUpdate?(state)
    }
    
    var value: T {
        return self._selector(_store.state)
    }

    var updateCount: Int {
        return _updateCount
    }

    func didUpdate(_ f: @escaping ((_ value: T) -> ()) ) {
        self._didUpdate = f
        self._store.subscribe(self) { $0.select(self._selector) }
    }

}


